# SysML2IA

This repository is the implementation of [1]. It includes (1) security-related interface automata, (2) decision procedures for the compositional non-deterministic noninterference in [2], (3) a translator from SysML behavior model to the security-related interface automata.

For the implementation of raw interface automata, please refer to [Open Interface Automata](https://bitbucket.org/suncong_xdu/open-interface-automata/src/master/)

The SysML behavior model should be built using Eclipse Papyrus v0.7 (As done on the CyCab model).

**New**: Thanks to Zongxu Zhang. A new translator for SysML 1.4 and the models of Papyrus v4.8.0 has been released.

**New**: SysML2IA has a new feature to test the conformance of interface automata following the principle of [3]. More information can be found in Jianfeng Pan's Master's thesis.

## Usage

* Run 
```
./deploy.sh
```
to build the *sysml2secia.jar* into *lib/*.

* To develop your own analysis:

1. import *test/* as Eclipse project.

2. import *lib/javaia.jar*, *lib/jdom-2.0.6.jar*, *lib/sysml2secia.jar* into the project.

3. refer to the documents and the test cases to develop your own analysis.

* The instructions of the new SysML2IA translator: *doc/new-sysmlia-instructions.docx*. 

* The instructions of the conformance testing: *doc/conformance-testing-instructions.docx*.

## References

1. Cong Sun, Ning Xi, Jinku Li, Qingsong Yao, Jianfeng Ma: Verifying Secure Interface Composition for Component-Based System Designs. APSEC (1) 2014: 359-366
2. Matias David Lee, Pedro R. D'Argenio: A Refinement Based Notion of Non-interference for Interface Automata: Compositionality, Decidability and Synthesis. SCCC 2010: 280-289
3. 	Jan Tretmans: Conformance Testing with Labelled Transition Systems: Implementation Relations and Test Generation. Comput. Networks ISDN Syst. 29(1): 49-79 (1996)

## Author

* **Cong Sun** - *School of Cyber Engineering, Xidian University* - [E-mail: suncong AT xidian DOT edu DOT cn]
* **Zongxu Zhang** - School of Cyber Engineering, Xidian University.
* **Jianfeng Pan** (Former member) - School of Cyber Engineering, Xidian University.

## License

* This library is licensed under the GPL 3.0 License.
* If it is used for academic publication, please cite our work [1].

