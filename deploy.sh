#!/bin/bash

#build the source code
cd src
make
mv sysml2secia.jar ../lib/
make clean

#generate java docs of interface automata library
javadoc -classpath ".:../lib/javaia.jar:../lib/jdom-2.0.6.jar" -subpackages -public -Xdoclint:html,syntax,accessibility org.xdu.nni org.xdu.sysmlia -d ../doc/

