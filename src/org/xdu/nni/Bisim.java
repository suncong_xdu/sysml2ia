package org.xdu.nni;

import java.util.*;
import org.xdu.jia.*;

public class Bisim{
	
	/**
	 * Define the weak bisimulation in Definition 4 of Matias Lee's SCCC'10 paper.
	 * @param ia1 The left-side interface automaton of relation R
	 * @param ia2 The right-side interface automaton of relation R
	 * @param relation The set of state pairs input to be confined recursively.
	 * @return The final weak bisimulation relation.
	 */
	private Rel Sim(InterfaceAutomaton ia1, InterfaceAutomaton ia2, Rel relation){
		Rel relset=new Rel();
Label:	for(StatePair sp: relation.rel){
			State q = sp.p;
			State q_ = sp.q;
			for(Action a: ia1.getActions(q)){ //(*)
				for(State r: ia1.post(q, a)){ //input deterministic to have a single "r" for (q,a)
					boolean r_correct=false;
Label2:				for(State p_: ia2.EpsilonClosure(q_)){
						for(State r_tmp: ia2.post(p_,a)) { //input deterministic to have a single "r_tmp" for (p_,a)
							for(State r_: ia2.EpsilonClosure(r_tmp)){
								if(relation.rel.contains(new StatePair(r,r_)) ){
									r_correct=true;
									break Label2; //go to Line(*) to ensure that for next "a", we also have (r,r_)\in relation.
								}
							}
						}
					}
					if(! r_correct){ //(**)
						/* forall p_\in epsilon(q_), forall (p_,a,rx)\in IA2, forall r_\in epsilon(rx),
						   we have (r, r_) \notin "relation" */
						continue Label; // find next pair of state (p,q)
					}
				}
			}
			//for all a\in IA1, we can find at least a (r,r_) for the (q,q_).
			for(Action a: ia2.getActions(q_)){
				for(State r_: ia2.post(q_, a)){
					boolean r_correct=false;
Label3:				for(State p: ia1.EpsilonClosure(q)){
						for(State r_tmp: ia1.post(p, a)){
							for(State r: ia1.EpsilonClosure(r_tmp)){
								if(relation.rel.contains(new StatePair(r,r_))){
									r_correct=true;
									break Label3;
								}
							}
						}
					}
					if(!r_correct){ //(***)
						continue Label;
					}
				}
			}
			//for all a\in IA2, we can find at least a (r,r_) for the (q,q_).
			relset.add(sp);
		}
		if(! relset.rel.containsAll(relation.rel) || ! relation.rel.containsAll(relset.rel)){
			// some state pair in "relation" can trigger Line (**) or Line (***), and fail to be added to "relset".
			return Sim(ia1, ia2, relset);
		}
		else{
			// all the elements in "relation" can be added to "relset".
			return relation;
		}
	}
/*	private Rel Sim(InterfaceAutomaton ia1, InterfaceAutomaton ia2, Rel relation){
		Rel relset=new Rel();
Label:	for(StatePair sp: relation.rel){
			State q=sp.p;
			State q_=sp.q;
			for(Action a: ia1.getActions(q)){
				for(State r: ia1.post(q, a)){
					boolean r_correct=false;
Label2:				for(State p_: ia2.EpsilonClosure(q_)){
						for(State r_:ia2.post(p_,a)){
							if(relation.rel.contains(new StatePair(r,r_)) ){
								r_correct=true;
								break Label2;
							}
						}
					}
					if(! r_correct){
						continue Label;
					}
				}
			}
			for(Action a: ia2.getActions(q_)){
				for(State r_: ia2.post(q_, a)){
					boolean r_correct=false;
Label3:				for(State p: ia1.EpsilonClosure(q)){
						for(State r: ia1.post(p, a)){
							if(relation.rel.contains(new StatePair(r,r_))){
								r_correct=true;
								break Label3;
							}
						}
					}
					if(!r_correct){
						continue Label;
					}
				}
			}
			relset.add(sp);
		}
		if(! relset.rel.containsAll(relation.rel) || ! relation.rel.containsAll(relset.rel)){
			return Sim(ia1, ia2, relset);
		}
		else{
			return relation;
		}
	}*/
	
	/**
	 * Decide if the two interface automata follow the weak bisimulation (R) in Def.4 of Matias Lee's SCCC'10 paper.
	 * @param ia1 The left-side interface automaton of relation R
	 * @param ia2 The right-side interface automaton of relation R
	 * @return true if (ia1 R ia2); false if !(ia1 R ia2).
	 */
	public boolean decide(InterfaceAutomaton ia1, InterfaceAutomaton ia2){
		Rel relation=new Rel();
		for(State s: ia1.getStates()){
			for(State t: ia2.getStates()){
				relation.add(new StatePair(s,t));
			}
		}
		return ( Sim(ia1,ia2,relation).rel.contains(new StatePair(ia1.getInit(),ia2.getInit())));
	}
	
	class StatePair{
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + ((q == null) ? 0 : q.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StatePair other = (StatePair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			if (q == null) {
				if (other.q != null)
					return false;
			} else if (!q.equals(other.q))
				return false;
			return true;
		}
		private State p;
		private State q;
		public StatePair(State p,State q){
			this.p=p;
			this.q=q;
		}
		private Bisim getOuterType() {
			return Bisim.this;
		}
	}
	class Rel{
		private Set<StatePair> rel=new HashSet<StatePair>();
		
		public boolean add(StatePair sp){
			return rel.add(sp);
		}
	}
}