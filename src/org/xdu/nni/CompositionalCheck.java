package org.xdu.nni;

import java.util.*;
import org.xdu.jia.*;

public class CompositionalCheck{
	/**
	 * If (1)this method return true for IA1 and IA2 /\ (2)IA1 is secure /\ (3)IA2 is secure, then IA1||IA2 is secure.
	 */
	public boolean HasReachableErrorStates(InterfaceAutomaton ia1, InterfaceAutomaton ia2){
		InterfaceAutomaton prod=ia1.ReducedProduct(ia2);
		Set<State> error_states=ia1.Illegal(ia2);
		
		return  prod.bTargetsReachableByTrans(prod.getInit(), error_states, prod.getTransitions());
	}
}