package org.xdu.nni;

import org.xdu.jia.*;
import java.util.*;

/**
 * Security-related interface automaton. We focus on deciding the security over total-order lattices.
 * Refer to "Cong Sun et al. Verifying Secure Interface Composition for Component-Based System Designs.
 * APSEC (1) 2014: 359-366" for detail of concepts.
 */	
public class SecIA{

	private String[] lattice;		// the total-order security lattice
	private InterfaceAutomaton ia;	// the automaton w.r.t. the security-related IA
	private List<IAPair> resList;	// the elements of R

	/**
	 * Constructor of security-related interface automaton.
	 * @param ia A regular interface automaton w.r.t this security-related IA.
	 * @param lat The total-order security lattice. the LOWEST level goes first and the HIGHEST goes last.
	 */
	public SecIA(InterfaceAutomaton ia, String[] lat){
		this.ia=ia;
		this.lattice=lat;
		this.resList=new ArrayList<IAPair>();
	}
	
	/**
	 * @return The list of pairs of interface automata. Each pair of interface automata defines an element of relation R (e.g. BSNNI, SIR-GNNI).
	 */
	public List<IAPair> getIAPairList(){
		return resList;
	}
	
	/**
	 * Given an interface automaton "S", and a set of actions "X",
	 * this method generates an interface automaton "S downarrow X".
	 * @param ia 	The input automaton "S"
	 * @param todo	The set of actions "X"
	 * @return		The result interface automaton "S downarrow X"
	 */
	private InterfaceAutomaton abstractOnActionSet(InterfaceAutomaton ia,Set<Action> todo){		
		InterfaceAutomaton res=new InterfaceAutomaton();
		for(State s: ia.getStates()){
			res.addState(s.getStr());
		}
		res.setInit(ia.getInit().getStr());
		for(Action a: ia.getInActions()){
			if(! todo.contains(a))
				res.addInAction(a);
		}
		for(Action a: ia.getOutActions()){
			if(! todo.contains(a)){
				res.addOutAction(a);
			}
		}
		for(Action a: ia.getHideActions()){
			res.addHideAction(a);
		}
		for(Action a: todo){
			res.addHideAction(a);
		}
		res.addTransitions(ia.getTransitions());
		return res;
	}
	
	/**
	 * Given an interface automaton "S", and a set of actions "X",
	 * this method generates an interface automaton "S uparrow X".
	 * @param ia	The input automaton "S"
	 * @param todo	The set of actions "X"
	 * @return		The result interface automaton "S uparrow X"
	 */
	private InterfaceAutomaton restrictOnActionSet(InterfaceAutomaton ia, Set<Action> todo){
		InterfaceAutomaton res=new InterfaceAutomaton();
		for(State s: ia.getStates()){
			res.addState(s.getStr());
		}
		res.setInit(ia.getInit().getStr());
		for(Action a: ia.getInActions()){
			if(! todo.contains(a))
				res.addInAction(a);
		}
		for(Action a: ia.getOutActions()){
			if(! todo.contains(a)){
				res.addOutAction(a);
			}
		}
		for(Action a: ia.getHideActions()){
			res.addHideAction(a);
		}
		for(Transition t: ia.getTransitions()){
			if(! todo.contains( t.getAct() ))
				res.addTransition(t);
		}
		return res;
	}
	
	/**
	 * Based on the input boundary "lvl", this method generates a pair of interface automata, and put it into R.
	 * @param level The boundary security level used to decide the element (pair of automata) of R.
	 * Note: the boundary level should not be specified to be the top level. For example, 
	 * if the security lattice is L&lt;H, we may set lvl=L.
	 * Then the hiding and restriction operations are performed on the input/output actions on H.
	 */
	public void GenerateBothSidesAtLevel(String lvl){
		int bound;
		for(bound=0; bound<this.lattice.length; bound++){
			if(this.lattice[bound].equals(lvl))
				break;
		}
		//Let G1 = \Gamma^{-1}_{L,I}(\nsqsubseteq level)
		//    G2 = \Gamma^{-1}_{L,O}(\nsqsubseteq level)
		//    G3 = \Gamma^{-1}_{L}(\nsqsubseteq level)
		Set<Action> G1=new HashSet<Action>();
		Set<Action> G2=new HashSet<Action>();
		for(int idx=bound+1; idx<this.lattice.length; idx++){
			String postfix = "_" + this.lattice[idx] + "_";
			for(Action a: ia.getInActions()){
				if(a.getStr().endsWith(postfix))
					G1.add(a);
			}
			for(Action a: ia.getOutActions()){
				if(a.getStr().endsWith(postfix)){
					G2.add(a);
				}
			}
		}
		Set<Action> G3=new HashSet<Action>();
		G3.addAll(G1);
		G3.addAll(G2);
		
		IAPair res=new IAPair(abstractOnActionSet( restrictOnActionSet(this.ia, G1), G2),
								abstractOnActionSet( this.ia, G3),
								lvl);
		resList.add(res);
	}
	
}
