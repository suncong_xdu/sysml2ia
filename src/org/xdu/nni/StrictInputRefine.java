package org.xdu.nni;

import java.util.*;
import org.xdu.jia.*;

public class StrictInputRefine{
	
	/**
	 * Define the SIR in Definition 6 of Matias Lee's SCCC'10 paper.
	 * @param ia1 The left-side interface automaton of relation R
	 * @param ia2 The right-side interface automaton of relation R
	 * @param relation The set of state pairs input to be confined recursively.
	 * @return The final SIR relation.
	 */
	private Rel Refine(InterfaceAutomaton ia1, InterfaceAutomaton ia2, Rel relation){
		Rel relset=new Rel();
Label:	for(StatePair sp: relation.rel){
			State q=sp.p;
			State q_=sp.q;
			for(Action a: ia1.getInActions(q)){
				for(State r: ia1.post(q,a)){
					boolean r_correct=false;
					for(State r_: ia2.post(q_,a)){
						if(relation.rel.contains(new StatePair(r,r_)) ){
							r_correct=true;
							break;
						}
					}
					if(! r_correct){
						continue Label;
					}
				}
			}//Here: condition (a) of SIR's definition is satisfied
			for(Action a: ia2.getInActions(q_)){
				for(State r_: ia2.post(q_,a)){
					boolean r_correct=false;
					for(State r: ia1.post(q,a)){
						if(relation.rel.contains(new StatePair(r,r_)) ){
							r_correct=true;
							break;
						}
					}
					if(!r_correct){
						continue Label;
					}
				}
			}// condition (b) of SIR is satisfied
			for(Action a: ia2.getOutActions(q_)){
				for(State r_: ia2.post(q_, a)){
					boolean r2_correct=false;
Label2:				for(State p: ia1.EpsilonClosure(q)){
						for(State r:ia1.post(p,a)){
							if(relation.rel.contains(new StatePair(r,r_)) ){
								r2_correct=true;
								break Label2;
							}
						}
					}
					if(! r2_correct){
						continue Label;
					}
				}
			}//Here: condition (c) of SIR is satisfied
			for(Action a: ia2.getHideActions(q_)){
				for(State r_: ia2.post(q_, a)){
					boolean r3_correct=false;
					for(State r: ia1.EpsilonClosure(q)){
						if(relation.rel.contains(new StatePair(r,r_)) ){
							r3_correct=true;
							break;
						}
					}
					if(! r3_correct){
						continue Label;
					}
				}
			}//Here condition (d) of SIR is satisfied
			relset.add(sp);
		}
		if(! relset.rel.containsAll(relation.rel) || ! relation.rel.containsAll(relset.rel)){
			//relation=relset;
			return Refine(ia1, ia2, relset);
		}
		else{
			return relation;
		}
	}
	
	/**
	 * Decide if the two interface automata follow the SIR relation (R) in Def.6 of Matias Lee's SCCC'10 paper.
	 * @param ia1 The left-side interface automaton of relation R
	 * @param ia2 The right-side interface automaton of relation R
	 * @return true if (ia1 R ia2); false if !(ia1 R ia2).
	 */
	public boolean decide(InterfaceAutomaton ia1, InterfaceAutomaton ia2){
		Rel relation=new Rel();
		for(State s: ia1.getStates()){
			for(State t: ia2.getStates()){
				relation.add(new StatePair(s,t));
			}
		}
		return ( Refine(ia1,ia2,relation).rel.contains(new StatePair(ia1.getInit(),ia2.getInit())));
	}
	
	//these inner classes used in the alternating simulation
	class StatePair{
		private State p;
		private State q;
		public StatePair(State p,State q){
			this.p=p;
			this.q=q;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + ((q == null) ? 0 : q.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StatePair other = (StatePair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			if (q == null) {
				if (other.q != null)
					return false;
			} else if (!q.equals(other.q))
				return false;
			return true;
		}
		private StrictInputRefine getOuterType() {
			return StrictInputRefine.this;
		}
	}
	class Rel{
		private Set<StatePair> rel=new HashSet<StatePair>();
		
		public boolean add(StatePair sp){
			return rel.add(sp);
		}
	}
}
