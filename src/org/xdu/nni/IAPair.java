package org.xdu.nni;

import org.xdu.jia.*;

/**
 * this class is used to store the pair of interface automata in the relation R. 
 * Different implementations of R (e.g. BSNNI, SIR-GNNI) take each pair of automata,
 * and decide on satisfaction of the relation R.
 */
public class IAPair{
	private String level;
	private InterfaceAutomaton left;
	private InterfaceAutomaton right;
	private boolean decision;
	
	/**
	 * Constructor of the pair of interface automata.
	 * @param left The first element of the pair of interface automata
	 * @param right The second element of the pair of interface automata
	 * @param lvl The security level on which we decide the satisfaction of R
	 */
	public IAPair(InterfaceAutomaton left, InterfaceAutomaton right, String lvl){
		this.left = left;
		this.right = right;
		this.level = lvl;
		this.decision = false;
	}
	
	/**
	 * Printer of the pair of interface automata.
	 */
	public void Print(){
		System.out.println("=================================");
		this.left.Print2();
		this.right.Print2();
		System.out.println("Decision at level " + this.level + ": " + this.decision );
		System.out.println("=================================");
	}
	
	/*The getter and setters*/
	public InterfaceAutomaton getLeft(){
		return left;
	}
	public InterfaceAutomaton getRight(){
		return right;
	}
	public String getLevel(){
		return level;
	}
	public boolean getDecision(){
		return decision;
	}
	public void setLeft(InterfaceAutomaton ia){
		this.left=ia;
	}
	public void setRight(InterfaceAutomaton ia){
		this.right=ia;
	}
	public void setLevel(String level){
		this.level=level;
	}
	public void setDecision(boolean b){
		this.decision=b;
	}
}