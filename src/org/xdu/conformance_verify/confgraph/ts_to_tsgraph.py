#!/usr/bin/env python

from ts import LTS
from ts import IOTS
from ts import STS
from ts import Testcase
from confgraph import TsGraph

import enum


def lts_to_graph(lts: LTS):
    """
    该函数根据LTS对象生成TsGraph对象

    :param lts: LTS对象
    """
    ts_graph = TsGraph()
    ts_graph.init_vertex = lts.init_state

    for state in lts.states:
        ts_graph.adjlists[state.state_name] = []
    for transition in lts.transitions:
        adj = (transition.end_state, transition.action)
        ts_graph.adjlists[transition.start_state].append(adj)

    return ts_graph


class ActionEnum(enum.Enum):
    """
    对IOTS系统中Action进行区分：
    0 表示输入动作
    1 表示输出动作
    2 表示内部动作
    """
    INPUT = 0
    OUTPUT = 1
    INTERNAL = 2

#mu qian you wen ti
def testcase_to_graph(testcase: Testcase) -> TsGraph:
    ts_graph = TsGraph()
    ts_graph.init_vertex = testcase.init_state
    list1 = list()
    list2 = list()

    for transition in testcase.transitions:
        first_state = transition.start_state
        action = transition.action
        second_state = transition.end_state
        if first_state not in list1:
            list1.append(first_state)
        if second_state not in list2:
            list1.append(first_state)

        # 设置动作的属性，是输入动作还是输出动作，或者是内部动作
        if action in testcase.output_actions:
            action.action_type = ActionEnum.OUTPUT
        elif action in testcase.input_actions:
            action.action_type = ActionEnum.INPUT
        else:
            action.action_type = ActionEnum.INTERNAL

        adj = (second_state, action)
        ts_graph.adjlists[first_state].append(adj)

    for state2 in list2:
        if state1 not in list1:
            ts_graph.adjlists[state2] = []


    return ts_graph

def iots_to_graph(iots: IOTS) -> TsGraph:
    """
    该函数根据IOTS对象生成TsGraph对象

    :param iots: IOTS对象
    """
    ts_graph = TsGraph()
    ts_graph.init_vertex = iots.init_state

    for state in iots.states:
        ts_graph.adjlists[state] = list()

    for transition in iots.transitions:
        first_state = transition.start_state
        action = transition.action
        second_state = transition.end_state

        # 设置动作的属性，是输入动作还是输出动作，或者是内部动作
        if action in iots.output_actions:
            action.action_type = ActionEnum.OUTPUT
        elif action in iots.input_actions:
            action.action_type = ActionEnum.INPUT
        else:
            action.action_type = ActionEnum.INTERNAL

        adj = (second_state, action)
        ts_graph.adjlists[first_state].append(adj)

    return ts_graph


def ts_to_graph(ts) -> TsGraph:
    """
    该函数综合不同迁移系统转换为ts graph

    :param ts: 迁移系统对象
    :return: 返回TsGraph对象
    """
    if type(ts) == LTS:
        return lts_to_graph(ts)
    elif type(ts) == IOTS:
        return iots_to_graph(ts)

