#!/usr/bin/env python3

"""
parsers 模块用来进行解析，它包含两个功能：

1、对uml文件进行解析，构建si文件
2、将si文件解析为对应的迁移系统

其中子模块siparsers完成功能1，子模块tsparsers完成功能2
"""

from .ts_parser import ts_parser
from .obtainsi import sts_transition_run
from .obtainsi import sts_transition_run1
