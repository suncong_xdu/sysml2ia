#!/usr/bin/env python3

from parsers.siparsers.core import UmlParser
from parsers.siparsers.func import parser_interaction
import re
import os
import sys


def convert_message(message_name):
    """将顺序图中的消息名转换为接口结构中的正确模式
    :param str message_name: 从uml文件中获取的消息名
    """
    pattern = '(.*)\(\){([HL][CI])}'
    match = re.match(pattern, message_name)

    if match:
        # 消息名
        function_name = match.group(1)
        # 安全属性：HI、LI表示高低完整性，HC、LC表示高低机密性
        secure_attr = match.group(2)
        if secure_attr == 'HC':
            return function_name + '_HC_'
        elif secure_attr == 'HI':
            return function_name + '_HI_'
        elif secure_attr == 'LC':
            return function_name + '_LC_'
        elif secure_attr == 'LI':
            return function_name + '_LI_'
    else:
        return message_name


def convert_message_fragment(msg_frag_obj, message_dict):
    """对mesage fragment进行解析，返回起相应的语句
    :param MessageFragemtn msg_frag_obj: MessageFragment对象
    :param Dict message_dict: 消息的id和name对
    """
    result = ''
    messageName = message_dict[msg_frag_obj.get_message()]

    if msg_frag_obj.get_name().endswith('SendEvent'):
        result += ('output ' + convert_message(messageName) + ' : ')
    elif msg_frag_obj.get_name().endswith('ReceiveEvent'):
        result += ('input ' + convert_message(messageName) + ' : ')
    elif msg_frag_obj.get_name() == 'hide':
        result += ('hide ' + convert_message(messageName) + ' : ')

    return result

def convert_message_fragment1(msg_frag_obj, message_dict):
    """对mesage fragment进行解析，返回起相应的语句
    :param MessageFragemtn msg_frag_obj: MessageFragment对象
    :param Dict message_dict: 消息的id和name对
    """
    result = ''
    messageName = message_dict[msg_frag_obj.get_message()]

    if msg_frag_obj.get_name().endswith('SendEvent'):
        result += ('output ' + convert_message(messageName) + '_HI_' + ' : ')
    elif msg_frag_obj.get_name().endswith('ReceiveEvent'):
        result += ('input ' + convert_message(messageName) + '_HI_' + ' : ')
    elif msg_frag_obj.get_name() == 'hide':
        result += ('hide ' + convert_message(messageName) + '_HI_' + ' : ')

    return result


def convert_lifeline_name(lifeline_name):
    """对生命线的Name进行格式化
    :param str lifeline_name: 生命线名字
    """
    if ':' in lifeline_name:
        return lifeline_name.split(':')[1]
    else:
        return lifeline_name


def lable_hide_message(fragments):
    """标记出为自身到自身的消息
    """
    lable = []
    index = 0
    while index < len(fragments) - 1:
        if fragments[index].get_name().endswith("SendEvent") and fragments[index + 1].get_name().endswith(
                'ReceiveEvent') and fragments[index].get_covered() == fragments[index + 1].get_covered():
            lable.append(index)
        index += 1

    result = fragments
    for index in lable:
        result[index].set_name('hide')
        result[index + 1] = None

    result = [item for item in result if item]
    return result


def construct_main(interactions_obj, lifeline_obj, message_dict):
    result = ''
    if "AirplaneAirCompressorSystem_sec" in interactions_obj.get_name():
        result += ('module ' + interactions_obj.get_name() + '_' \
                 + convert_lifeline_name(lifeline_obj.get_name()) + ':\n')
    elif "AirplaneAirCompressorSystem" in interactions_obj.get_name():
        result += ('module-lts ' + interactions_obj.get_name() + '_' \
                 + convert_lifeline_name(lifeline_obj.get_name()) + ':\n')
    else:
        result += ('module ' + interactions_obj.get_name() + '_' \
                   + convert_lifeline_name(lifeline_obj.get_name()) + ':\n')
    result += 'initial : s0\n'
    count = 1
    cur = 0
    fragments = interactions_obj.get_fragments()
    fragments = lable_hide_message(fragments)
    for frag in fragments:
        if frag.get_type() == 'uml:MessageOccurrenceSpecification' and frag.get_covered() == lifeline_obj.get_id():
            result += convert_message_fragment(frag, message_dict)
            result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
            cur = count
            count += 1
        elif frag.get_type() == 'uml:CombinedFragment':
            operands = frag.get_operand()
            if frag.get_interaction_operator() == 'opt':
                msg_frag = operands[0].get_message_fragment()
                msg_frag = lable_hide_message(msg_frag)
                guard = operands[0].get_guard()
                specificatin = guard.get_specification()

                if_cur = cur  # 记录if时候的状态位置
                if_action = 'hide ' + specificatin.get_name() + '_is_' + specificatin.get_value() + ' : '
                if_action += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                result += if_action
                cur = count
                count += 1
                for msg in msg_frag:
                    if msg.getCovered() == lifeline_obj.get_id():
                        result += convert_message_fragment(msg, message_dict)
                        result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                        cur = count
                        count += 1
                if_end_action = 'hide ' + specificatin.get_name() + '_is_not_' + specificatin.get_value() + ' : '
                if_end_action += ('{%s -> %s}\n' % ('s' + str(if_cur), 's' + str(cur)))
                result += if_end_action
                cur = count
                count += 1
            elif frag.get_interaction_operator() == 'alt':
                specificatin = operands[0].get_guard().get_specification()
                msg_frag0 = operands[0].get_message_fragment()  # if 片段内的消息
                msg_frag0 = lable_hide_message(msg_frag0)
                msg_frag1 = operands[1].get_message_fragment()  # else 片段内的消息
                msg_frag1 = lable_hide_message(msg_frag1)
                if_cur = cur  # 记录if时候的状态位置
                if_action = 'hide ' + specificatin.get_name() + '_is_' + specificatin.get_value() + ' : '
                if_action += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                result += if_action
                cur = count
                count += 1
                for msg in msg_frag0:
                    if msg.getCovered() == lifeline_obj.getId():
                        result += convert_message_fragment(msg, message_dict)
                        result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                        cur = count
                        count += 1
                end_if_cur = cur
                else_action = 'hide ' + specificatin.get_name() + '_is_not_' + specificatin.get_value() + ' : '
                else_action += ('{%s -> %s}\n' % ('s' + str(if_cur), 's' + str(end_if_cur + 1)))
                result += else_action
                cur = count
                count += 1
                index = 0
                msg_attach_lifeline = [msg for msg in msg_frag1 if msg.get_covered() == lifeline_obj.get_id()]
                for msg in msg_attach_lifeline:
                    if index != (len(msg_attach_lifeline) - 1):
                        result += convert_message_fragment(msg, message_dict)
                        result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                        cur = count
                        count += 1
                    elif index == (len(msg_attach_lifeline) - 1):
                        result += convert_message_fragment(msg, message_dict)
                        result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(end_if_cur)))
                        cur = end_if_cur
                    index += 1

    result += 'endmodule'
    return result

def construct_next(interactions_obj, lifeline_obj, message_dict):
    result = ''
    result += ('module ' + interactions_obj.get_name() + '_' \
               + convert_lifeline_name(lifeline_obj.get_name()) + ':\n')
    result += 'initial : s0\n'
    count = 1
    cur = 0
    fragments = interactions_obj.get_fragments()
    fragments = [item for item in fragments if item]
    for frag in fragments:

        if frag.getType() == 'uml:MessageOccurrenceSpecification' and frag.get_covered() == lifeline_obj.get_id():
            result += convert_message_fragment(frag, message_dict)
            result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
            cur = count
            count += 1
        elif frag.get_type() == 'uml:CombinedFragment':
            operands = frag.get_operand()
            for operand in operands:
                frag_in_oprand = operand.get_message_fragment()
                frag_in_oprand = [item for item in frag_in_oprand if item]
                for msg in frag_in_oprand:
                    if msg.get_covered() == lifeline_obj.get_id():
                        result += convert_message_fragment(msg, message_dict)
                        result += ('{%s -> %s}\n' % ('s' + str(cur), 's' + str(count)))
                        cur = count
                        count += 1

    result += 'endmodule'
    return result


def parse_sequence_diagram_name(seq_name):
    """解析输入的顺序图的名字，将顺序图的名字以类名和函数名分开
    :param str seq_name: 顺序图名
    :return Tuple
    """
    class_ = ['AP_AdvancedFailsafe_Copter', 'NavEKF', 'AP_MotorsMulticopter', 'AP_Notify', 'AP_Rally_Copter',
              'AP_Arming_Copter', 'AutoYaw', 'GCS_MAVLINK_Copter', 'AP_Avoidance_Copter', 'Copter', 'AC_PosControl',
              'AC_AttitudeControl_Multi', 'AC_AttitudeControl', 'AP_Motors_Multicopter', 'RC_Channel', 'ModeStabilize',
              'ModeAltHold', 'ModeRTL', 'ModeLand', 'ModeGuided', 'Mode', 'AP_Motors', '_TakeOff', 'AP_AHRS_DCM',
              'AP_AHRS']
    need_class = ''
    for item in class_:
        if item in seq_name:
            need_class = item
            break
    if need_class == "RC_Channel" and 'RC_Channels' in seq_name:
        need_class = 'RC_Channels'

    pattern = '(' + need_class + ')_(.*)'
    match = re.match(pattern, seq_name)
    if not match:
        return None, seq_name

    return match.group(1), match.group(2)


def sts_transition_run(interaction_name, model_file):
    UML_FILE_PATH = model_file
    # SEQ_NAME = 'auto_takeoff_attitude_run'
    SEQ_CLASS, SEQ_NAME = parse_sequence_diagram_name(interaction_name)

    print('[*] Input uml path: %s' % UML_FILE_PATH)

    print('[*] Input Sequence diagram name: %s' % interaction_name)
    parser = UmlParser(UML_FILE_PATH)
    # 获取到uml中intraction对象
    uml_interactions = parser.get_uml_interaction()

    # 根据SEQ_NAME找到匹配的interaction
    spec_interacs = [item for item in uml_interactions if item.getAttribute('name') == SEQ_NAME]
    interactions_obj = parser_interaction(spec_interacs[0])
    message_dict = interactions_obj.message_by_id()
    lifeline_dict = interactions_obj.lifeline_by_id()
    lifelines = interactions_obj.get_lifelines()

    write_to_file = ''
    write_to_file += construct_main(interactions_obj, lifelines[0], message_dict)
    write_to_file += '\n'

    save_file_name = interaction_name + '.si'
    save_file_path = os.path.join('testcase_examples', save_file_name)

    with open(save_file_path, mode='w') as f:
        f.write(write_to_file)

    print('[+] Generate file: %s' % save_file_path)

def sts_transition_run1(interaction_name, model_file):
    UML_FILE_PATH = model_file
    # SEQ_NAME = 'auto_takeoff_attitude_run'
    SEQ_CLASS, SEQ_NAME = parse_sequence_diagram_name(interaction_name)

    print('[*] Input uml path: %s' % UML_FILE_PATH)

    print('[*] Input Sequence diagram name: %s' % interaction_name)
    parser = UmlParser(UML_FILE_PATH)
    # 获取到uml中intraction对象
    uml_interactions = parser.get_uml_interaction()

    # 根据SEQ_NAME找到匹配的interaction
    spec_interacs = [item for item in uml_interactions if item.getAttribute('name') == SEQ_NAME]
    interactions_obj = parser_interaction(spec_interacs[0])
    message_dict = interactions_obj.message_by_id()
    lifeline_dict = interactions_obj.lifeline_by_id()
    lifelines = interactions_obj.get_lifelines()

    write_to_file = ''
    write_to_file += construct_main1(interactions_obj, lifelines[0], message_dict)
    write_to_file += '\n'

    save_file_name = interaction_name + '.si'
    save_file_path = os.path.join('testcase_examples', save_file_name)

    with open(save_file_path, mode='w') as f:
        f.write(write_to_file)

    print('[+] Generate file: %s' % save_file_path)




