#!/usr/bin/env python3

from ts import LTS
from parsers.tsparsers import parse_ts_type
from constants import TS_TYPE
from parsers.tsparsers import lts_parser
from parsers.tsparsers import iots_parser
from parsers.tsparsers import sts_parser


def ts_parser(file_name: str) -> LTS:
    return iots_parser(file_name)

