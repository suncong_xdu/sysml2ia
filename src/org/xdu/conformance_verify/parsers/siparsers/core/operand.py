#!/usr/bin/env python3

from parsers.siparsers.core import UmlObject


class Operand(UmlObject):
    """\
        表示组合片段中的operand
    """

    def __init__(self, name, id_, type_, covered):
        """
        :param str name: 名字
        :param str id: id
        :param str type_: 类型
        :param str coverd: 覆盖的生命线
        """
        super(Operand, self).__init__(type_, id_, name)
        self._covered = covered
        self._message_fragment = []
        self._guard = None

    def get_message_fragment(self):
        return self._message_fragment

    def add_message_fragment(self, message_fragment_obj):
        self._message_fragment.append(message_fragment_obj)

    def get_guard(self):
        return self._guard

    def set_guard(self, guard_obj):
        self._guard = guard_obj


class Guard(UmlObject):
    """Operand 中的guard对象的表示
    """

    def __init__(self, name, id_, type_):
        """
        :param str name: 名字
        :param str id_: id
        :param str type_: 类型
        """
        super(Guard, self).__init__(type_, id_, name)
        self._specification = None

    def get_specification(self):
        return self._specification

    def set_specification(self, spec_obj):
        self._specification = spec_obj


class Specification(UmlObject):
    """guard中的specification对象
    """

    def __init__(self, name, id_, type_, value):
        """
        :param str id: id
        :param str type_: 类型
        :param str value: 条件值
        """
        super(Specification, self).__init__(type_, id_, name)
        self._value = value

    def get_value(self):
        return self._value
