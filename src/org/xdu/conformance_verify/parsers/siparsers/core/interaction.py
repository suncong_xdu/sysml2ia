#!/usr/bin/env python3

from parsers.siparsers.core import UmlObject


class Interaction(UmlObject):
    """表示interaction对象
    """
    def __init__(self, type_, id_, name):
        super(Interaction, self).__init__(type_, id_, name)
        self._lifelines = [] # interaction 中lifeline队形
        self._fragments = [] # interaction中的fragment对象
        self._messages = [] # interaction中的message对象

    def get_lifelines(self):
        return self._lifelines

    def get_fragments(self):
        return self._fragments

    def get_message(self):
        return self._messages

    def add_lifeline(self, lifeline_obj):
        self._lifelines.append(lifeline_obj)

    def add_fragment(self, fragment_obj):
        self._fragments.append(fragment_obj)

    def add_message(self, message_obj):
        self._messages.append(message_obj)

    def lifeline_by_id(self):
        """
        :return: 返回一个字典，其中key为生命线的id，value为生命线的的name
        """
        lifeline_dict = {}
        for item in self._lifelines:
            lifeline_dict[item.get_id()] = item.get_name()

        return lifeline_dict

    def message_by_id(self):
        """
        :return: 返回一个字典，其中key为消息的id，value为消息的name
        """
        message_dict = {}
        for item in self._messages:
            message_dict[item.get_id()] = item.get_name()

        return message_dict
