#!/usr/bin/env python3

import xml.dom.minidom as xdom


class UmlParser(object):
    """
    对uml文件进行解析，获取uml文件与顺序图有关的相关的信息
    """
    def __init__(self, uml_file):
        """
        :param str uml_file: The path of uml file
        """
        self._uml_file = uml_file

    def get_uml_interaction(self):
        """获取uml文件中与顺序图相关的片段
        :return list: 一个保存所有顺序图节点信息的列表
        """
        doc = xdom.parse(self._uml_file)
        # 获取uml中根节点xmi：XMI
        # xmi_xmi = doc.getElementsByTagName('xmi:XMI')
        # 或者xmi:XMI下的uml:Model节点
        # uml_model = xmi_xmi[0].getElementsByTagName('uml:Model')
        uml_model = doc.getElementsByTagName('uml:Model')
        # 货主uml:Model下的packageImport节点
        packaged_element = uml_model[0].getElementsByTagName('packagedElement')
        # 获取与顺序图相关的节点信息，uml_interaction返回一个信息的列表
        uml_interaction = []
        for p in packaged_element:
            if p.attributes['xmi:type'].value == 'uml:Interaction':
                uml_interaction.append(p)
        return uml_interaction

    def get_uml_file(self):
        return self._uml_file
