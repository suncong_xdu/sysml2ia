#!/usr/bin/env python3


class UmlObject(object):
    """
    UML文件各组件之间共同的部分：type、name、id
    """

    def __init__(self, type_, id_, name):
        """
        param str type_: 类型
        param str id_: id
        param str name: 名字
        """
        self._type = type_
        self._id = id_
        self._name = name

    def get_type(self):
        return self._type

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name
