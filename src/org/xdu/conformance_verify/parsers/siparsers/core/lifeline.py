#!/usr/bin/env python3

from parsers.siparsers.core import UmlObject


class Lifeline(UmlObject):
    """表示生命线的类
    """

    def __init__(self, name, id_, type_, covered_by):
        """构造函数
        :param str name: 生命线的名字
        :param str id_: 生命线对应的id
        :param str type_: 类型
        :param str covered_by: 覆盖
        """
        super(Lifeline, self).__init__(type_, id_, name)
        self._covered_by = covered_by

    def get_covered_by(self):
        return self._covered_by

