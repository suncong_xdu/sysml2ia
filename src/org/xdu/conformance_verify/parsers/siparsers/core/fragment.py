#!/usr/bin/env python3

from parsers.siparsers.core import UmlObject


class Fragment(UmlObject):
    """表示uml中fragment中共同属性的部分
    """
    def __init__(self, type_, id_, name, covered):
        """用来记录顺序图中fragment的相关信息
        :param str name: fragment的名字
        :param str type_: fragment对象的类型
        :param str id_: 对象的id
        :param str covered: 覆盖到的生命线id
        """
        super(Fragment, self).__init__(type_, id_, name)
        self._covered = covered

    def get_covered(self):
        return self._covered


class MessageFragment(Fragment):
    """表示message的fragment
    """
    def __init__(self,type_, id_, name, covered, message):
        """
        :param str name: 名字
        :param str type_: 类型
        :param str id_: id
        :param str coverd: 覆盖到的生命线id
        :param str message: message的id
        """
        super(MessageFragment, self).__init__(type_, id_, name, covered)
        self._message = message

    def get_message(self):
        return self._message


class CombinedFragment(Fragment):
    """表示combined的Fragment
    """
    def __init__(self, type_, id_, name, covered, interaction_operator):
        """
        :param str name: 名字
        :param str type_: 类型
        :param str id_: id
        :param str covered: 覆盖到的生命线id
        :param str interaction_operator: combined的类型
        """
        super(CombinedFragment, self).__init__(type_, id_, name, covered)
        self._interaction_operator = interaction_operator
        self._operand = []  # 保存该类型fragment中operand

    def get_interaction_operator(self):
        return self._interaction_operator

    def add_operand(self, operand_obj):
        self._operand.append(operand_obj)

    def get_operand(self):
        return self._operand

