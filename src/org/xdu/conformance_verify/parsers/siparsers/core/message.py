#!/usr/bin/env python3

from parsers.siparsers.core import UmlObject


class Message(UmlObject):
    """表示消息的类
    """
    def __init__(self, type_, id_, name, receive_event, send_event):
        """Contructor
        :param str name: 消息的名字
        :param str id_: 消息的id
        :param str receive_event: 消息发送方覆盖
        :param str send_event: 消息接收方覆盖
        """
        super(Message, self).__init__(type_, id_, name)
        self._send_event = send_event
        self._receiveEvent = send_event

    def get_send_event(self):
        return self._send_event

    def get_receive_event(self):
        return self._receive_event
