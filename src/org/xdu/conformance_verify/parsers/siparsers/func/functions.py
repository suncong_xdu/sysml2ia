#!/usr/bin/env python3

import sys
import os

sys.path.append(os.getcwd())

from parsers.siparsers.core import Lifeline
from parsers.siparsers.core import Message
from parsers.siparsers.core import MessageFragment
from parsers.siparsers.core import CombinedFragment
from parsers.siparsers.core import Guard
from parsers.siparsers.core import Specification
from parsers.siparsers.core import Operand
from parsers.siparsers.core import Interaction

"""\
    该模块中函数是用来对uml文件中节点对象进行解析的函数，并且构造相应的节点对象
"""


# 解析uml中节点对象的相同的属性：type、id、name
def parser_common(uml_obj):
    type_ = uml_obj.getAttribute('xmi:type')
    id_ = uml_obj.getAttribute('xmi:id')
    name = uml_obj.getAttribute('name')
    return type_, id_, name


# 解析lifeline节点对象信息，并返回Lifeline对象
def parser_lifeline(uml_lifeline):
    type_, id_, name = parser_common(uml_lifeline)
    covered_by = uml_lifeline.getAttribute('coveredBy')
    return Lifeline(name, id_, type_, covered_by)


# 解析message节点对象信息，并返回Message对象
def parser_message(uml_message):
    type_, id_, name = parser_common(uml_message)
    recv = uml_message.getAttribute('receiveEvent')
    send = uml_message.getAttribute('sendEvent')
    return Message(type_, id_, name, recv, send)


# 解析message fragment对象信息，并返回Messagefragemtn对象
def parser_msg_frag(uml_msg_frag):
    type_, id_, name = parser_common(uml_msg_frag)
    covered = uml_msg_frag.getAttribute('covered')
    message = uml_msg_frag.getAttribute('message')
    return MessageFragment(type_, id_, name, covered, message)


# 解析specification对象信息，并返回Specification对象
def parser_specification(uml_spec):
    type_, id_, name = parser_common(uml_spec)
    value = uml_spec.getAttribute('value')
    return Specification(name, id_, type_, value)


# 解析guard对象信息， 并返回Guard对象
def parser_guard(uml_guard):
    type_, id_, name = parser_common(uml_guard)
    guard_obj = Guard(name, id_, type_)
    uml_specs = uml_guard.getElementsByTagName('specification')
    if uml_specs:
        guard_obj.set_specification(parser_specification(uml_specs[0]))
    return guard_obj


# 解析operand 对象信息，并且返回Operand对象
def parser_operand(uml_operand):
    type_, id_, name = parser_common(uml_operand)
    covered = uml_operand.getAttribute('covered')
    operand_obj = Operand(name, id_, type_, covered)
    uml_fragments = uml_operand.getElementsByTagName('fragment')
    for frag in uml_fragments:
        operand_obj.add_message_fragment(parser_msg_frag(frag))

    uml_guard = uml_operand.getElementsByTagName('guard')[0]
    operand_obj.set_guard(parser_guard(uml_guard))

    return operand_obj


# 解析combined fragment对象,并返回CombinedFragment对象
def parser_combined_frag(uml_combined_frag):
    type_, id_, name = parser_common(uml_combined_frag)
    covered = uml_combined_frag.getAttribute('covered')
    interaction_operator = uml_combined_frag.getAttribute('interactionOperator')
    frag_obj = CombinedFragment(type_, id_, name, covered, interaction_operator)
    uml_operands = uml_combined_frag.getElementsByTagName('operand')
    for operand in uml_operands:
        frag_obj.add_operand(parser_operand(operand))

    return frag_obj


# 删除ActionExecutionSpecification的fragment
def _delete_action_fragment(uml_fragments):
    result = [item for item in uml_fragments if
              not item.getAttribute('name').startswith("ActionExecutionSpecification")]
    return result


# 对象uml interaction对象中fragment对象进行清洗，删除重复的部分
def _clean_fragment(uml_interac):
    uml_fragments = list(uml_interac.getElementsByTagName('fragment'))
    uml_fragments = _delete_action_fragment(uml_fragments)
    combined_fragment = [frag for frag in uml_fragments if
                         frag.getAttribute('xmi:type') == 'uml:CombinedFragment']
    msg_frag_in_combined = []
    for combined_frag in combined_fragment:
        msg_frag_in_combined.extend(_delete_action_fragment(combined_frag.getElementsByTagName('fragment')))

    # 在uml_fragments 删除msg_frag_in_combined中的fragment
    for frag in msg_frag_in_combined:
        uml_fragments.remove(frag)

    return uml_fragments


# 解析interaction对象，并返回Interaction对象
def parser_interaction(uml_interac):
    type_, id_, name = parser_common(uml_interac)
    interac_obj = Interaction(type_, id_, name)
    # 添加消息对象
    uml_messages = uml_interac.getElementsByTagName('message')
    for mess in uml_messages:
        interac_obj.add_message(parser_message(mess))

    # 添加生命线对象
    uml_lifelines = uml_interac.getElementsByTagName('lifeline')
    for lifeline in uml_lifelines:
        interac_obj.add_lifeline(parser_lifeline(lifeline))

    # 添加fragment对象
    uml_fragments = _clean_fragment(uml_interac)
    for frag in uml_fragments:
        if frag.getAttribute('xmi:type') == 'uml:MessageOccurrenceSpecification':
            interac_obj.add_fragment(parser_msg_frag(frag))
        elif frag.getAttribute('xmi:type') == 'uml:CombinedFragment':
            interac_obj.add_fragment(parser_combined_frag(frag))

    return interac_obj

