#!/usr/bin/env python3

from queue import Queue,LifoQueue,PriorityQueue
from ts import Testcase
from ts import IOTS
from confgraph import iots_to_graph
from confgraph import testcase_to_graph
from confgraph import TsGraph
from core import State


def testcase_run(testcase: Testcase, imp_iots:IOTS) -> bool:
    """
    一个测试用例和实现执行
    :param testcase: 一个测试用例对象
    :param imp_iots: 实现迁移系统对象
    :return: 验证通过为True；反之，为假
    """
    iots_graph = iots_to_graph(imp_iots)
    testcase_graph = testcase_to_graph(testcase)
    if not iots_graph or not testcase_graph:
        return False

    gq = Queue(maxsize=0)
    new_graph = TsGraph()
    new_graph.init_vertex = testcase_graph.init_vertex
    for k,v in testcase_graph.adjlists.items():
        if k.state_name == testcase_graph.init_vertex.state_name:
            key = k
    com = (iots_graph.init_vertex,key,new_graph)
    gq.put(com)
    while gq.empty() != True:
        gq_items = gq.get()
        vertex1 = gq_items[0]
        vertex2 = gq_items[1]
        new_graph = gq_items[2]
        for second_state, act in iots_graph.adjlists[vertex1]:
            for s_state, a in testcase_graph.adjlists[vertex2]:
                if str(act) == str(a):
                    adj = (s_state, a)
                    new_graph.adjlists[vertex2].append(adj)
                    for k,v in iots_graph.adjlists.items():
                        if k.state_name == second_state.state_name:
                            key1 = k
                            break
                        else:
                            continue
                    for k,v in testcase_graph.adjlists.items():
                        if str(k.state_name) == str(s_state.state_name):
                            new_graph.adjlists[k] = []
                            key2 = k
                            obj = (key1, key2, new_graph)
                            gq.put(obj)
                            break
                        else:
                            new_graph.adjlists[s_state] = []
                            continue
    for k,v in new_graph.adjlists.items():
        if not v:
            k_obj = State(k)
            if testcase.verdict[k] != 1:
                return False
    return True


