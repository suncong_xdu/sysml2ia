#!/usr/bin/env python3

"""
测试用例生成模块
"""
import random
import copy
from queue import Queue,LifoQueue,PriorityQueue
from ts import IOTS
from ts import Testcase
from ts.ts_tools import get_end_state_with_hide_action
from conf.conf_tools import get_init
from ts.ts_tools import obtian_subset_from_iots
from typing import List
from confgraph import iots_to_graph
from confgraph import TsGraph
from confgraph import ActionEnum
from core import Transition
from core import State
from core import Action
import itertools
import copy


def iots_testcase_generate(iots: IOTS) -> Testcase:
    """
    该函数根据iots对象生成testcase
    """
    init_state = iots.init_state
    states = iots.states
    acts = iots.actions
    ins = iots.input_actions
    outs = iots.output_actions
    transitions = iots.transitions
    verdict = dict()

    init = get_init(iots, iots.init_state)  # init(iots.init_state)
    hide_transitions_start_with_init_state = get_end_state_with_hide_action(iots, iots.init_state)
    iots_subset = obtian_subset_from_iots(iots)

    return Testcase(
        init_state,
        states,
        acts,
        ins,
        outs,
        transitions,
        verdict
    )


def tsgraph_to_testcase(tsgraph:TsGraph) -> Testcase:
    """
    将TsGraph表示的迁移系统转换为Testcase对象
    :param tsgraph: TsGraph对象
    :return Testcase对象
    """
    root = tsgraph.init_vertex
    if not root:
        return None
    init_state = root
    states = [State(root)]
    acts = []
    ins = []
    outs = []
    transitions = []
    verdict = {}
    # 用来判断是否重合的状态
    adjust_s = list()
    adjust_acts = list()
    adjust_ins = list()
    adjust_outs = list()
    for k, v in tsgraph.adjlists.items():
        if k not in adjust_s:
            verdict[k] = 0
            adjust_s.append(k)
            states.append(k)
        if not v:
            verdict[k] = 1
        for second_state, act in v:
            acts.append(act)
            if second_state not in adjust_s:
                adjust_s.append(second_state)
                states.append(second_state)
            if act.action_type == ActionEnum.OUTPUT:
                if act not in adjust_outs:
                    adjust_outs.append(act)
                    outs.append(act)
            elif act.action_type == ActionEnum.INPUT:
                if act not in adjust_ins:
                    adjust_ins.append(act)
                    ins.append(act)
            transitions.append(Transition(k, act, second_state))
    # 这里需要添加判断状态是pass还是fail的函数，当前全设置为1
    #for s in states:
    #    verdict[s] = 1

    return Testcase(init_state,
                    states,
                    acts,
                    ins,
                    outs,
                    transitions,
                    verdict
                    )

def testcases_generate(spec_iots:IOTS) -> List[Testcase]:
    """
    执行测试用例生成算法number次
    :param spec_iots: 规范的IOTS对象
    :param number: 执行次数
    :return: Testcase的集合
    """
    # 将IOTS对象转换为TSGraph对象
    iots_graph = iots_to_graph(spec_iots)
    # TsGraph是空树
    if not iots_graph.init_vertex:
        return list()

    if not iots_graph.adjlists[iots_graph.init_vertex]:
        return [spec_iots]

    gq = Queue(maxsize=0)
    new_graph = TsGraph()
    new_graph.init_vertex = iots_graph.init_vertex
    sub_graphs = []
    leafs1 = []
    leafs1.append(iots_graph.init_vertex)
    obj = (new_graph, leafs1)
    gq.put(obj)
    while gq.empty() != True:
        gqitems = gq.get()
        g = gqitems[0]
        leafs = gqitems[1]
        for k,v in iots_graph.adjlists.items():
            if not v:
                continue
            if k in leafs:
                combinations = []
                for i in range(1, len(v)+1):
                    for c in itertools.combinations(v, i):
                        combinations.append(list(c))
                for combination in combinations:
                    gg = TsGraph()
                    gg.adjlists = copy.deepcopy(g.adjlists)
                    gg.init_vertex = g.init_vertex
                    for trans in combination:
                        if trans not in gg.adjlists[k]:
                    	    gg.adjlists[k].append(trans)
                    leafs2 = []
                    for nei, act in combination:
                    	if nei not in leafs and nei not in leafs1:
                            gg.adjlists[nei] = list()
                            leafs2.append(nei)
                    for t in leafs:
                        if t.state_name != k.state_name and t not in leafs1:
                            leafs2.append(t)
                    if gg not in sub_graphs:
                        sub_graphs.append(gg)
                        obj = (gg, leafs2)
                        gq.put(obj)
                if k not in leafs1:
                    leafs1.append(k)
    return [tsgraph_to_testcase(graph) for graph in sub_graphs]

