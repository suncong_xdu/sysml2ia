#!/usr/bin/env python3

"""
一致性测试函数
"""

from ts import IOTS
from conf import testcases_generate
from conf import testcase_run


def conformance_verify(spec_iots: IOTS, imp_iots: IOTS, testcases_filename='testcases.txt') -> bool:
    """
    一致性验证
    :param testcases_filename:
    :param spec_iots: 规范的IOTS对象
    :param imp_iots: 实现的IOTS对象
    :param number: 测试用例生成算法执行次数
    :return: 验证通过返回True；反之，返回Fasle
    """
    testcases = testcases_generate(spec_iots)
    with open(testcases_filename, 'w') as f:
        for testcase in testcases:
            f.write(str(testcase) + '\n')

    # 测试用例集运行，判断是否一致性
    for ts in testcases:
        if not testcase_run(ts, imp_iots):
            return False

    return True
