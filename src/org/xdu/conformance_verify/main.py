import argparse
import time

from conf import conformance_verify
from parsers import ts_parser

info_string = """\
一致性检测工具的命令如下：
secure_and_conformance_testing --conformance -s 规范接口自动机文件 -i 实现接口自动机文件
"""

def args_parser():
    parser = argparse.ArgumentParser(prog="conformance_testing")
    parser.add_argument("--conformance", help="一致性测试子命令", action="store_true")
    parser.add_argument("-s", help="指定规范输入输出迁移系统文件")
    parser.add_argument("-i", help="指定实现输入输出迁移系统文件")
    parser.add_argument("-test", help="用于测试程序，指定输出的测试用例集文件名称,默认为testcases.txt")
    args = parser.parse_args()
    return args

def main():
    start_time = time.time()
    args = args_parser()

    if args.conformance:
        # 一致性测试部分
        result = False
        spec = args.s
        imp = args.i
        spec_iots = ts_parser(spec)
        imp_iots = ts_parser(imp)
        testcases_filename = "testcases.txt"
        if args.test:
            testcases_filename = args.test
        result = conformance_verify(spec_iots, imp_iots, testcases_filename)

        if result:
            print("[+] Conformance verify: True.")
        else:
            print("[+] Conformance verify: False.")
    else:
        # 输出命令选项
        print(info_string)

    end_time = time.time()

    print("[+] 验证花费的时间：{}".format(end_time - start_time))


if __name__ == "__main__":
    main()