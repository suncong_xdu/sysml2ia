package org.xdu.sysmlia;

import org.xdu.jia.*;
import java.util.*;
import java.io.*;

public class IAGenerator{
	private List<InterfaceAutomaton> ia_list=new ArrayList<InterfaceAutomaton>();
	
	/*this algorithm is similar to SDtoIA in ISSE'11*/
	private void constructIA_aux(MsgFrag obj, SeqDiag sd, Set<Transition> trans, Set<State> states){
		if(obj instanceof CombinedFrag){
			for(MsgFrag m: ((CombinedFrag)obj).list ){
				constructIA_aux(m,sd,trans,states);
			}
		}
		if(obj instanceof Message){
			CombinedFrag loopi=sd.LastMsgOfLoop((Message)obj);
			if(loopi!=null){ //obj is the last of loopi
				//System.out.print("aaaa:"); loopi.Print();
				//System.out.print("bbbb:"); ((Message)obj).Print(); System.out.println();
								
				Message first= sd.getFirstMsgOfCombinedFrag(loopi).get(0);
				
				Message previous=sd.getPreviousMsgOfCombinedFrag(loopi, (Message)obj);
				/*****************************/
				/*System.out.print("first:");
				if(first!=null)
					first.Print();
				System.out.println();
				System.out.print("previous:");
				if(previous!=null)
					previous.Print();
				System.out.println();*/
				/*********************************/
				
				String fr="";
				String to="";
				if(first!=null){
					to=first.getFrom();
				} else {
					to=((Message)obj).getTo();
				}
				if(previous!=null){
					fr=previous.getTo();
				} else {
					fr=((Message)obj).getFrom();
				}
				
				states.add(new State(fr));
				states.add(new State(to));
				trans.add( new Transition(fr, ((Message)obj).getAction(), to) );
			}
			else {
				CombinedFrag fragi=sd.FirstMsgOfCombinedFrag((Message)obj);
				if(fragi!=null){ // obj is the first message of some fragment
					//System.out.print("aaaa:"); fragi.Print();
					//System.out.print("bbbb:"); ((Message)obj).Print(); System.out.println();
					
					Message previous=sd.getPreviousMsgOfCombinedFrag(fragi,(Message)obj);
					/* note: here cannot deal with a condition that
					 * several alt messages are the first ones of a fragment*/
					String fr="";
					if(previous!=null){
						fr=previous.getTo();
					} else {
						fr=((Message)obj).getFrom();
					}
					String to=((Message)obj).getTo();
					states.add(new State(fr));
					states.add(new State(to));
					trans.add(new Transition(fr, ((Message)obj).getAction() ,to));
				} else {
					Message previous=sd.getPreviousMsg((Message)obj);
					String fr="";
					if(previous!=null){
						fr=previous.getTo();
					} else {
						fr=((Message)obj).getFrom();
					}
					String to=((Message)obj).getTo();
					states.add(new State(fr));
					states.add(new State(to));
					trans.add(new Transition(fr, ((Message)obj).getAction() ,to));
				}
			}
		}
	}
	
	private InterfaceAutomaton constructIA(SeqDiag sd, MsgFrag obj){
		InterfaceAutomaton res=new InterfaceAutomaton();
		res.setInit( sd.getInitMessage().getFrom() );
		for(Message s: sd.getMessages()){
			if(s.getFrom().indexOf("Environment:") != -1)
				res.addInAction(new Action(s.getAction()));
			else if(s.getTo().indexOf("Environment:") != -1)
				res.addOutAction(new Action(s.getAction()));
			else if(s.getFrom().equals(s.getTo()))
				res.addHideAction(new Action(s.getAction()));
		}
		constructIA_aux(obj,sd,res.getTransitions(),res.getStates());
		return res;
	}
	
	public void GenerateIAs(String pathname){
		try {
			SeqDiagrams sds=new SeqDiagrams(pathname);
			String destname;
			if(pathname.indexOf(".uml")!=-1)
				destname= pathname.replaceAll(".uml", ".si");
			else
				destname=pathname.concat(".si");
			FileWriter out=new FileWriter(destname);
			for(FragSeqdiagPair pair: sds.getFragSeqdiagPairs()){
				//pair.sd.Print();
				ia_list.add( this.constructIA(pair.sd,pair.frag) );
				String outstring= ( this.constructIA(pair.sd,pair.frag) ).
						generateIAOutputString2(this.getRandomString(8));
				System.out.println(outstring);
				out.write(outstring);
				out.write("\n");
			}
			out.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private String getRandomString(int length) { //length��ʾ����ַ�ĳ���
	    String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";   
	    Random random = new Random();   
	    StringBuffer sb = new StringBuffer();   
	    for (int i = 0; i < length; i++) {   
	        int number = random.nextInt(base.length());   
	        sb.append(base.charAt(number));   
	    }   
	    return sb.toString();   
	 }  
	
}