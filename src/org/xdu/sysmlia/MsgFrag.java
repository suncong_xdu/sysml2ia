package org.xdu.sysmlia;
/**
 * This is the parent class of Message and CombinedFrag
 * @author Cong Sun
 */
public class MsgFrag{
	TFrag type;
	
	public void Print(){
		if(this instanceof Message)
			((Message)this).Print();
		if(this instanceof CombinedFrag)
			((CombinedFrag)this).Print();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MsgFrag other = (MsgFrag) obj;
		if (type != other.type)
			return false;
		return true;
	}
}

/**
 * The enumeration of the type of MsgFrags
 * @author Cong Sun
 */
enum TFrag{
	Loop, Alt, Seq, Message, Root
};