package org.xdu.sysmlia;
import java.io.IOException;
import java.util.*;
import java.util.regex.*;

//import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
//import org.jdom2.util.IteratorIterable;
import org.jdom2.output.XMLOutputter;

public class SeqDiagrams{
	private List<FragSeqdiagPair> pair_list=new ArrayList<FragSeqdiagPair>();
	private static int msg_order_no=0; //used to record the order of message in each SeqDiag
	
	/**
	 * @param parents
	 * @param pName
	 * @return the children elements of a parent, the name of parent is <pName>
	 */
	private List<Element> getChildElementByParentName(List<Element> parents, String pName){
		for(Element e:parents){
			if(e.getName().equals(pName))
				return e.getChildren();
		}
		return null;
	}
	
	/**
	 * select from the <contents> the elements whose name is <name>
	 * @param contents
	 * @param name
	 * @return
	 */
	private List<Element> selectContentsByName(List<Element> contents, String name){
		List<Element> res=new ArrayList<Element>();
		for(Element e: contents){
			if (e.getName().equals(name)){
				res.add(e);
			}
		}
		return res;
	}
	
	/**
	 * used for debugging
	 * @param packagedElems
	 * @return
	 */
	private List<Element> getElementsWithOwnedBehavior(List<Element> packagedElems){
		List<Element> elementsWithOwnedBehavior=new ArrayList<Element>();
		for(Element e: packagedElems){
			List<Attribute> alist=e.getAttributes();
			for(Attribute attr: alist){
				if(attr.getName().equals("type") && 
					attr.getValue().equals("uml:Class") &&
					e.getChild("ownedBehavior")!=null){ //only "uml:Class" model elements can have sequence diagram
					elementsWithOwnedBehavior.add(e);
				}
			}
		}
		return elementsWithOwnedBehavior;
	}
	
	/**
	 * used for debugging, replace the labels with easily identified ones
	 * @param text
	 * @return
	 */
	private String replaceLabels(String text){
		String regex="\"[_]+[-_0-9a-zA-Z]+\"";
		Matcher m=Pattern.compile(regex).matcher(text);
		StringBuffer res=new StringBuffer();
		while(m.find()){
			String matchedText=m.group();
			String label=NameMapping.map.get(matchedText);
			if(label==null){
				label="\"L"+NameMapping.id++ +"\"";
				//label="L"+NameMapping.id++;
				NameMapping.map.put(matchedText, label);
			}
			m.appendReplacement(res, label);
		}
		m.appendTail(res);
		
		String regex1="\"[_]+[-_0-9a-zA-Z]+([ _]+[-_0-9a-zA-Z]+)*\"";
		m=Pattern.compile(regex1).matcher(res.toString());
		res=new StringBuffer();
		while(m.find()){
			StringTokenizer s=new StringTokenizer(m.group());
			StringBuilder l=new StringBuilder("\"");
			while(s.hasMoreTokens()){
				String currentToken=s.nextToken();
				String label=NameMapping.map.get(currentToken);
				if(label==null){
					label="L"+NameMapping.id++;
					NameMapping.map.put(currentToken, label);
				}
				l.append(label).append(" ");
			}
			l.replace(l.length()-1, l.length()-1, "\"");
			m.appendReplacement(res, l.toString());
		}
		m.appendTail(res);
		return res.toString();
	}
	
	private Map<String,String> FindLifelines(Element elem){
		Map<String,String> idnamemap=new HashMap<String,String>();
		for(Element e:elem.getChildren("lifeline")){
			String s_att="", s_name="";
			for(Attribute att: e.getAttributes() ){
				if(att.getName().equals("id"))
					s_att=att.getValue();
				if(att.getName().equals("name")){
					s_name=att.getValue();
				}
			}
			idnamemap.put(s_att, s_name );
		}
		return idnamemap;
	}
	
	/**
	 * @param elem a specific element for message
	 * @return the correspondence between the message id and the message name
	 */
	private Map<String,String> FindMessages(Element elem){
		Map<String,String> idnamemap=new HashMap<String,String>();
		//construct a map whose key is the signature of each message, and the value is the correspondence of <msg_id, msg_name>
		Map<String,IdNamePair> sigmap=new HashMap<String,IdNamePair>();
		for(Element e:elem.getChildren("message")){
			String s_att="", s_name="", s_sig="";
			boolean bTrueMsg=false;
			for(Attribute att: e.getAttributes() ){
				if(att.getName().equals("signature"))
					s_sig=att.getValue();
				if(att.getName().equals("id"))
					s_att=att.getValue();
				if(att.getName().equals("name")){
					s_name=att.getValue();
				}
				if(att.getName().equals("receiveEvent") || att.getName().equals("sendEvent"))
					bTrueMsg=true;
			}
			if(bTrueMsg){
				sigmap.put(s_sig, new IdNamePair(s_att, s_name) );
			}
		}
		for(IdNamePair p: sigmap.values()){
			idnamemap.put(p.getId(),p.getName());
		}
		return idnamemap;
	}
	
	private String getCombinedFragType(Element fragelem){
		boolean bfrag=false;
		for(Attribute att: fragelem.getAttributes()){
			if(att.getName().equals("type") && att.getValue().equals("uml:CombinedFragment")){
				bfrag=true;
			}
		}
		if(bfrag){
			for(Attribute att: fragelem.getAttributes()){
				if(att.getName().equals("interactionOperator") )
					return att.getValue();					
			}
			return new String("seq");
		}
		return new String("");//the sequential combined fragments do not have attribute "interactionOperator"
	}
	
	private boolean bMessageType(Element elem){
		for(Attribute att: elem.getAttributes()){
			if(att.getName().equals("type") && att.getValue().equals("uml:MessageOccurrenceSpecification"))
				return true;
		}
		return false;
	}
	
	/**
	 * @param msgs the incomplete message information from the model
	 * @param lifelines the life-line information mapping from component id to component name
	 * @param msg_idname the inner message mapping from message id to message name
	 * @return a mapping from message id to the complete message with order information
	 */
	private Map<String,Message> MessageConstruct(
					List<Element> msgs, Map<String,String> lifelines, Map<String,String> msg_idname){
		Map<String,InnerMsgInfo> inner_msgs=new TreeMap<String, InnerMsgInfo>();
		for(Element msg: msgs){ // for each message event in a fragment
			InnerMsgInfo info=new InnerMsgInfo();
			String id="";
			for(Attribute att:msg.getAttributes()){
				if(att.getName().equals("message")){
					info.messageid=msg_idname.get( att.getValue() );
				}
				if(att.getName().equals("name")){
					if(att.getValue().indexOf("MessageSend")!=-1){
						info.bSend=true;
					} else {
						info.bSend=false;
					}
				}
				if(att.getName().equals("covered")){
					info.obj=lifelines.get(att.getValue());
				}
				if(att.getName().equals("id")){
					id=att.getValue();
					//add to the name mapping
					String label=NameMapping.map.get(id);
					if(label==null){
						label="_L"+NameMapping.id++;
						NameMapping.map.put(id, label);
					}
				}
			}
			if(inner_msgs.get(id)==null){
				info.order=msg_order_no++;
				//if(info.obj==null)
				//	System.out.println("aaabbbccc");
				info.obj=info.obj.concat(NameMapping.map.get(id));
				inner_msgs.put(id, info);
			}
		}
		Map<String,Message> target_msgs=new TreeMap<String,Message>();
		for(InnerMsgInfo info:inner_msgs.values()){
			if(target_msgs.get(info.messageid)==null)
				target_msgs.put(info.messageid, new Message());
		}
		for(InnerMsgInfo info:inner_msgs.values()){
			Message m=target_msgs.get(info.messageid);
			if(m==null) //this won't happen
				continue;
			if(info.bSend){
				m.setFrom(info.obj);
			} else {
				m.setTo(info.obj);
			}
			m.setAction(info.messageid);
			//for the message order:
			if( (m.getOrder()==0) ||
				(info.order < m.getOrder()) ){
				m.setOrder(info.order);
			}
		}
		return target_msgs;
	}
	
	/**
	 * recursively construct the sub-fragments, used by ConstructSeqDiags()
	 */
	private MsgFrag constructSubFragments(SeqDiag sd, Element element,TFrag ty, 
			Map<String,String> lifelines, Map<String,String> msg_idname){
		MsgFrag frag;
		if(ty==TFrag.Alt || ty==TFrag.Loop || ty==TFrag.Seq || ty==TFrag.Root)
			frag=new CombinedFrag(ty);
		else
			frag=new Message("-","-","-"); //unusual for the regular models
		
		List<Element> msgOccurs =new ArrayList<Element>();
		for(Element elem:element.getChildren()){
			if(elem.getName().equals("fragment") && getCombinedFragType(elem).equals("loop")){
				//use elem.getChildren().get(0) means jump into the single child <operand>
				MsgFrag tmpfrag=constructSubFragments(sd,elem.getChildren().get(0),TFrag.Loop,lifelines,msg_idname);
				tmpfrag.type=TFrag.Loop;
				((CombinedFrag)frag).AddFrag(tmpfrag);
				sd.getLoopList().AddFrag(tmpfrag);
			}
			if(elem.getName().equals("fragment") && getCombinedFragType(elem).equals("alt")){
				MsgFrag tmpfrag=constructSubFragments(sd,elem.getChildren().get(0),TFrag.Alt,lifelines,msg_idname);
				tmpfrag.type=TFrag.Alt;
				((CombinedFrag)frag).AddFrag(tmpfrag);
				sd.getAltList().AddFrag(tmpfrag);
			}
			if(elem.getName().equals("fragment") && getCombinedFragType(elem).equals("seq")){
				MsgFrag tmpfrag=constructSubFragments(sd,elem.getChildren().get(0),TFrag.Seq,lifelines,msg_idname);
				tmpfrag.type=TFrag.Seq;
				((CombinedFrag)frag).AddFrag(tmpfrag);
				sd.getSeqList().AddFrag(tmpfrag);
			}
			if(elem.getName().equals("fragment") && bMessageType(elem)){ //messages
				boolean hasMsgField=false;
				for(Attribute att: elem.getAttributes()){
					if(att.getName().equals("message")){
						hasMsgField=true;
						break;
					}
				}
				if(hasMsgField)
					msgOccurs.add(elem);
			}
		}
		//Here: construct the messages
		//the order preserved by the msgOccurs is indeed the occurrence order of message in the model,
		//the method MessageConstruct should preserve this order and reflect this order to the target_msgs
		Map<String,Message> target_msgs=MessageConstruct(msgOccurs,lifelines,msg_idname);
		if(frag instanceof CombinedFrag){
			for(Message s:target_msgs.values()){
				((CombinedFrag) frag).AddFrag(s);
				sd.addMessage(s);
			}
		}
		return frag;
	}
	
	/**
	 * construct the sequential diagrams using the lifeline map and the message map
	 */
	private void ConstructSeqDiag(Element elem,Map<String,String> lifelines, Map<String,String> msg_idname){
		FragSeqdiagPair pair=new FragSeqdiagPair();
		msg_order_no=1;
		pair.frag=constructSubFragments(pair.sd,elem,TFrag.Root,lifelines,msg_idname);
		//pair.frag.Print();//for debug
		pair.sd.adjustAltMessages();
		pair.sd.deriveInitialMessage();
		
		pair_list.add(pair);
	}
	
	/**
	 * construct the SeqDiagrams Object from a specific model file
	 * @param modelpath the pathname of the model file
	 */
	public SeqDiagrams(String modelpath){
		try{
			SAXBuilder jdomBuilder = new SAXBuilder();
			Document jdomDocument = jdomBuilder.build(modelpath);
	
			Element body = jdomDocument.getRootElement();//xmi
			List<Element> bodyContent=body.getChildren();
			List<Element> modelContent=getChildElementByParentName(bodyContent,"Model");//uml:Model
			List<Element> packagedElements=selectContentsByName(modelContent, "packagedElement");//packagedElement
			
			List<Element> ownedBehaviors=new ArrayList<Element>();
			for(Element p: packagedElements){
				//find in packagedElements who contain the "ownedBehavior" tag
				ownedBehaviors.addAll(selectContentsByName(p.getChildren(),"ownedBehavior") );
			}
	
			for(Element seqDiag: ownedBehaviors){
				
				/*used to output the useful segments of xml model
				XMLOutputter out=new XMLOutputter(); // for debug
				System.out.println(replaceLabels(out.outputString(seqDiag)));*/
				/*************************/
				Map<String,String> lifeline_idname=FindLifelines(seqDiag);
				
				Map<String,String> msg_idname=FindMessages(seqDiag);

				ConstructSeqDiag(seqDiag,lifeline_idname,msg_idname);

			}
		} catch (JDOMException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public void Print(){
		for(FragSeqdiagPair pair: pair_list){
			pair.sd.Print();
		}
	}
	
	public List<FragSeqdiagPair> getFragSeqdiagPairs(){
		return this.pair_list;
	}

	class IdNamePair{
		private String id;
		private String name;
		
		public IdNamePair(String i,String n){
			id=i;
			name=n;
		}
		public String getId(){
			return id;
		}
		public String getName(){
			return name;
		}
	}

	class InnerMsgInfo{
		boolean bSend; //true: send; false: receive
		String obj; //the sender/receiver object
		String messageid; //the message identifier
		int order=0; //record the order of message
		
		public void Print(){
			System.out.println("bSend:"+bSend+"; obj:"+obj+"; messageid:"+messageid);
		}
	}
	
}

class NameMapping{
	static Map<String,String> map=new HashMap<String,String>();
	static int id=0;
}

/**
 * store the correspondence of the object list and the sequential diagram
 * @author Cong Sun
 */
class FragSeqdiagPair{
	MsgFrag frag=new MsgFrag();
	SeqDiag sd=new SeqDiag();
}