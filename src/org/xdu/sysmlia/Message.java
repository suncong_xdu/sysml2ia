package org.xdu.sysmlia;

/**
 * The message w.r.t the ones in sequential diagrams
 * @author Cong Sun
 *
 */
public class Message extends MsgFrag{
	private String from="";
	private String to="";
	private String action="";
	private int order=0;
	
	public Message(String frm, String act, String to){
		this.from=frm;
		this.to=to;
		this.action=act;
		this.type=TFrag.Message;
	}
	
	public Message(){
		this.from=this.to=this.action="";
		this.type=TFrag.Message;
	}
	
	public String getFrom(){
		return from;
	}
	public String getTo(){
		return to;
	}
	public String getAction(){
		return action;
	}
	public int getOrder(){
		return order;
	}
	public void setOrder(int ord){
		this.order=ord;
	}
	public void setFrom(String s){
		from=s;
	}
	public void setTo(String s){
		to=s;
	}
	public void setAction(String s){
		action=s;
	}
	public void Print(){
		System.out.print("<("+this.order+")"+this.from+";"+this.action+";"+this.to+">");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + order;
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (order != other.order)
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
}