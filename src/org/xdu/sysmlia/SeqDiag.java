package org.xdu.sysmlia;
import java.util.*;

/**
 * The class for each sequential diagram in the model
 * @author Cong Sun
 */
public class SeqDiag{
	private final int ORDER_MAX=100000;
	private Message init;
	private Set<Message> msgs;
	private CombinedFrag loop_list;
	private CombinedFrag alt_list;
	private CombinedFrag seq_list;
	
	public SeqDiag(){
		init=null;
		msgs=new HashSet<Message>();
		loop_list=new CombinedFrag(TFrag.Loop);
		alt_list=new CombinedFrag(TFrag.Alt);
		seq_list=new CombinedFrag(TFrag.Seq);
	}
	
	public CombinedFrag getLoopList(){
		return loop_list;
	}
	
	public CombinedFrag getAltList(){
		return alt_list;
	}
	
	public CombinedFrag getSeqList(){
		return seq_list;
	}
	
	public void setInitMessage(Message s){
		init=s;
	}
	public Message getInitMessage(){
		return init;
	}
	
	public void addMessage(Message s){
		msgs.add(s);
	}
	public Set<Message> getMessages(){
		return msgs;
	}
	
	/**
	 * adjst the messages in each alt fragment to have 
	 * the same &lt;from&gt;, the same &lt;to&gt;, and the same &lt;order number&gt;
	 */
	public void adjustAltMessages(){
		for(MsgFrag mf: alt_list.list){
			int order=-1;
			String fr="";
			String to="";
			for(MsgFrag mf2: ((CombinedFrag)mf).list){
				if(mf2 instanceof Message){
					if(((Message) mf2).getOrder() > order)
						order= ((Message) mf2).getOrder();
					fr= ((Message)mf2).getFrom();
					to= ((Message)mf2).getTo();
				}
			}
			for(MsgFrag mf2: ((CombinedFrag)mf).list){
				if(mf2 instanceof Message){
					((Message) mf2).setOrder(order);
					((Message) mf2).setFrom(fr);
					((Message) mf2).setTo(to);
				}
			}
		}
	}
	/*public void adjustAltMessages(){
		for(MsgFrag mf: alt_list.list){
			int order=ORDER_MAX;
			String fr="";
			String to="";
			for(MsgFrag mf2: ((CombinedFrag)mf).list){
				if(mf2 instanceof Message){
					if(((Message) mf2).getOrder() < order)
						order= ((Message) mf2).getOrder();
					fr= ((Message)mf2).getFrom();
					to= ((Message)mf2).getTo();
				}
			}
			for(MsgFrag mf2: ((CombinedFrag)mf).list){
				if(mf2 instanceof Message){
					((Message) mf2).setOrder(order);
					((Message) mf2).setFrom(fr);
					((Message) mf2).setTo(to);
				}
			}
		}
	}*/
	
	/**
	 * derive the initial message for SeqDiag after the set of messages &lt;msgs&gt; has completed.
	 */
	public void deriveInitialMessage(){
		int order=0;
		Message tmp=null;
		for(Message m: msgs){
			if(m.getOrder()> order){
				order=m.getOrder();
				tmp=m;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(),tmp.getAction(),tmp.getTo());
			res.setOrder(order);
			this.setInitMessage(res);
		}
	}
	/*	public void deriveInitialMessage(){
		int order=ORDER_MAX;
		Message tmp=null;
		for(Message m: msgs){
			if(m.getOrder()< order){
				order=m.getOrder();
				tmp=m;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(),tmp.getAction(),tmp.getTo());
			res.setOrder(order);
			this.setInitMessage(res);
		}
	}*/
	
	public List<Message> getLastMsgOfCombinedFrag(CombinedFrag cf){
		Set<Message> msgs=new HashSet<Message>();
		getAllMessagesOfCombinedFrag(cf,msgs);
		int order=ORDER_MAX;
		//Message tmp=null;
		for(Message p: msgs){
			if(order > p.getOrder()){
			//	tmp=p;
				order=p.getOrder();
			}
		}
		List<Message> lasts=new ArrayList<Message>();
		if(order!=ORDER_MAX){
			//Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			//res.setOrder(tmp.getOrder());
			for(Message p: msgs){
				if(p.getOrder()==order){
					Message tmp=new Message(p.getFrom(),p.getAction(),p.getTo());
					tmp.setOrder(p.getOrder());
					lasts.add(tmp);
				}					
			}
		}
		return lasts;
	}
	/**
	 * @param cf a specific combined fragment
	 * @param m a specific message
	 * @return whether m is the last message in the combined fragment cf
	 */
	private boolean bLastMsgsOfCombinedFrag(CombinedFrag cf, final Message m){
		if(cf==null || cf.list.isEmpty())
			return false;
		List<Message> lasts=getLastMsgOfCombinedFrag(cf);
		for(Message last: lasts){
			if(last.equals(m))
				return true;
		}
		return false;
	}
	/*private boolean bLastMsgsOfCombinedFrag(CombinedFrag cf, final Message m){
		if(cf==null || cf.list.isEmpty())
			return false;
		MsgFrag mf=cf.list.get(cf.list.size()-1);
		if(mf instanceof CombinedFrag){
			return bLastMsgsOfCombinedFrag( (CombinedFrag)mf, m);
		}
		if(mf instanceof Message){
			if(cf.type==TFrag.Loop || cf.type== TFrag.Seq) {
				if( ((Message)mf).equals(m))
					return true;
			}
			if(cf.type==TFrag.Alt){
				List<Message> l=new ArrayList<Message>();
				l.add((Message)mf);
				for(int i=cf.list.size()-2; i>=0; i--){ // trace back for all the alternative msgs
					if(cf.list.get(i) instanceof Message ){
						Message tmp= (Message) (cf.list.get(i));
						if(tmp.getOrder()== ((Message)mf).getOrder() )
							l.add( tmp );
					}
					//else
					//	break;
				}
				if(l.contains(m))
					return true;
			}
		}
		return false;//m not contained
	}*/
	
	/**
	 * @param m a specific message
	 * @return a specific combined fragment &lt;loop_i&gt; when the message &lt;m&gt; is in the last object of &lt;loop_i&gt;
	 */
	public CombinedFrag LastMsgOfLoop(Message m){
		for( MsgFrag loopi: loop_list.list ){
			if(bLastMsgsOfCombinedFrag((CombinedFrag)loopi,m))
				return (CombinedFrag)loopi;
		}
		return null;
	}
	
	/**
	 * @param cf a specific combined fragment
	 * @return the first message in the combined fragment cf
	 */
	public List<Message> getFirstMsgOfCombinedFrag(CombinedFrag cf){
		Set<Message> msgs=new HashSet<Message>();
		getAllMessagesOfCombinedFrag(cf,msgs);
		int order=-1;
		//Message tmp=null;
		for(Message p: msgs){
			if(order < p.getOrder()){
				//tmp=p;
				order=p.getOrder();
			}
		}
		List<Message> resset=new ArrayList<Message>();
		if(order!=-1){
			//Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			//res.setOrder(tmp.getOrder());
			for(Message p: msgs){
				if(p.getOrder()== order){
					Message tmp=new Message(p.getFrom(),p.getAction(),p.getTo());
					tmp.setOrder(p.getOrder());
					resset.add(tmp);
				}
			}
			//return resset;
		}
		return resset;
	}
	/*public Message getFirstMsgOfCombinedFrag(CombinedFrag cf){
		MsgFrag mf=cf.list.get(0);
		if(mf instanceof CombinedFrag){
			return getFirstMsgOfCombinedFrag( (CombinedFrag)mf );
		}
		if(mf instanceof Message){
			return (Message)mf;
		}
		return null;//won't happen
	}*/
	
	private void getAllMessagesOfCombinedFrag(CombinedFrag cf, Set<Message> msgs){
		for(MsgFrag mf: cf.list){
			if(mf instanceof CombinedFrag){
				getAllMessagesOfCombinedFrag((CombinedFrag)mf,msgs);
			}
			else if(mf instanceof Message){
				msgs.add((Message)mf);
			}
		}
	}
	/**
	 * @param cf a specific combined fragment
	 * @param m a specific message
	 * @return the previous message of m in cf
	 */
	public Message getPreviousMsgOfCombinedFrag(CombinedFrag cf, final Message m){
		Set<Message> msgs=new HashSet<Message>();
		getAllMessagesOfCombinedFrag(cf,msgs);
		if(! msgs.contains(m)){
			System.out.println(m.toString()+" not in the combined fragment");
			return null;
		}
		int order=ORDER_MAX;
		Message tmp=null;
		for(Message p: msgs){
			if(p.getOrder() <= m.getOrder())
				continue;
			if(p.getOrder() < order){
				order=p.getOrder();
				tmp=p;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			res.setOrder(tmp.getOrder());
			return res;
		}
		return null;
	}
	/*public Message getPreviousMsgOfCombinedFrag(CombinedFrag cf, final Message m){
		Set<Message> msgs=new HashSet<Message>();
		getAllMessagesOfCombinedFrag(cf,msgs);
		if(! msgs.contains(m)){
			System.out.println(m.toString()+" not in the combined fragment");
			return null;
		}
		int order=-1;
		Message tmp=null;
		for(Message p: msgs){
			if(p.getOrder() >= m.getOrder())
				continue;
			if(p.getOrder() > order){
				order=p.getOrder();
				tmp=p;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			res.setOrder(tmp.getOrder());
			return res;
		}
		return null;
	}*/

	private boolean bFirstMsgOfCombinedFrag(CombinedFrag cf, final Message m){
		if(cf==null || cf.list.isEmpty())
			return false;
		List<Message> firsts=getFirstMsgOfCombinedFrag(cf);
		for(Message f: firsts){
			if(f.equals(m))
				return true;
		}
		return false;//m not contained
	}
	/*private boolean bFirstMsgOfCombinedFrag(CombinedFrag cf, final Message m){
		if(cf==null || cf.list.isEmpty())
			return false;
		MsgFrag mf=cf.list.get(0);
		if(mf instanceof CombinedFrag){
			return bFirstMsgOfCombinedFrag( (CombinedFrag)mf, m);
		}
		if(mf instanceof Message){
			if(cf.type==TFrag.Loop || cf.type== TFrag.Seq) {
				if( ((Message)mf).equals(m))
					return true;
			}
			if(cf.type==TFrag.Alt){
				List<Message> l=new ArrayList<Message>();
				l.add((Message)mf);
				for(int i=1; i<cf.list.size(); i++){ // trace back for all the alternative msgs
					if(cf.list.get(i) instanceof Message) {
						Message tmp= (Message) (cf.list.get(i) );
						if(tmp.getOrder() == ((Message)mf).getOrder())
							l.add( tmp );
					}
					//else
					//	break;
				}
				if(l.contains(m))
					return true;
			}
		}
		return false;//m not contained
	}*/
	
	/**
	 * @param m a specific message
	 * @return the combined fragment when m is the first message of that fragment
	 */
	public CombinedFrag FirstMsgOfCombinedFrag(Message m){
		for( MsgFrag loopi: loop_list.list ){
			if(bFirstMsgOfCombinedFrag((CombinedFrag)loopi,m))
				return (CombinedFrag)loopi;
		}
		for( MsgFrag alti: alt_list.list ){
			if(bFirstMsgOfCombinedFrag((CombinedFrag)alti,m))
				return (CombinedFrag)alti;
		}
		for( MsgFrag seqi: seq_list.list ){
			if(bFirstMsgOfCombinedFrag((CombinedFrag)seqi,m))
				return (CombinedFrag)seqi;
		}
		return null;
	}

	/**
	 * @param m a specific message
	 * @return the previous message of m
	 */
	public Message getPreviousMsg(final Message m){
		if(! this.msgs.contains(m))
			return null;
		int order=ORDER_MAX;
		Message tmp=null;
		for(Message p: this.msgs){
			if(p.getOrder() <= m.getOrder())
				continue;
			if(p.getOrder() < order){
				order=p.getOrder();
				tmp=p;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			res.setOrder(tmp.getOrder());
			return res;
		}
		return null;
	}
	/*public Message getPreviousMsg(final Message m){
		if(! this.msgs.contains(m))
			return null;
		int order=-1;
		Message tmp=null;
		for(Message p: this.msgs){
			if(p.getOrder() >= m.getOrder())
				continue;
			if(p.getOrder() > order){
				order=p.getOrder();
				tmp=p;
			}
		}
		if(tmp!=null){
			Message res=new Message(tmp.getFrom(), tmp.getAction(), tmp.getTo());
			res.setOrder(tmp.getOrder());
			return res;
		}
		return null;
	}*/
	
	public void Print(){
		System.out.println("SeqDiag: {");
		init.Print();
		System.out.print(";\n[");
		for(Message s:this.msgs){
			s.Print();
			System.out.print(";");
		}
		System.out.println("];");
		this.loop_list.Print();
		this.alt_list.Print();
		this.seq_list.Print();		
	}
}