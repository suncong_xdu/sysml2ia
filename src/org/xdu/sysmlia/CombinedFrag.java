package org.xdu.sysmlia;

import java.util.*;

/**
 * Combined Fragment containing a list of other fragments
 * @author Cong Sun
 */
public class CombinedFrag extends MsgFrag{
	List<MsgFrag> list;
	
	public CombinedFrag(TFrag ty){
		type=ty;
		list=new ArrayList<MsgFrag>();
	}
	
	public void AddFrag(MsgFrag frag){
		list.add(frag);
	}
	
	public boolean valid(){
		if(this.type==TFrag.Loop && list.size()>=1)
			return true;
		else if((this.type==TFrag.Alt || this.type==TFrag.Seq) && list.size()>=2)
			return true;
		else return false;
	}
	
	public void Print(){
		switch(this.type){
		case Loop: System.out.println("CombinedFrag[loop]{"); break;
		case Alt: System.out.println("CombinedFrag[alt]{"); break;
		case Seq: System.out.println("CombinedFrag[seq]{"); break;
		case Message: System.out.println("CombinedFrag[msg]{"); break;
		case Root: System.out.println("CombinedFrag[root]{"); break;
		default: System.out.println("CombinedFrag[error]{"); break;
		}
		for(MsgFrag m:list){
			if(m instanceof Message)
				System.out.println(
						"<("+((Message)m).getOrder()+")"+((Message)m).getFrom()+";"+((Message)m).getAction()+";"+((Message)m).getTo()+">");
			if(m instanceof CombinedFrag)
				m.Print();
		}
		System.out.println("}");
	}
}