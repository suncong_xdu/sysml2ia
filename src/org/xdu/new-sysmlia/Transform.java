package core;

import com.uppaal.model.core2.*;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.internal.impl.*;

import java.io.*;
import java.util.*;

public class Transform {

    public static ArrayList<Locati> locatis;

    public static void getAllLocation(ArrayList<Locati> locatiArrayList){
        locatis =new ArrayList<>();
        ArrayList<Locati> p= locatiArrayList;
        if(p.size()!=0){
            Queue<Locati> queue = new LinkedList<>();
            for(int k=0;k< p.size();k++){
                queue.offer(p.get(k));
            }
            while (!queue.isEmpty()){
                Locati poll = queue.poll();
                if(!locatis.contains(poll)){
                    locatis.add(poll);
                }
                //System.out.println(poll.getMessage());
                for(Locati locati1 :poll.getNexts()){
                    queue.offer(locati1);
                }
            }
        }
    }

    public static void disPlay(ArrayList<Locati> locatis){
        ArrayList<Locati> p= locatis;
        if(p.size()!=0){
            Queue<Locati> queue = new LinkedList<>();
            for(int k=0;k< p.size();k++){
                queue.offer(p.get(k));
            }
            while (!queue.isEmpty()){
                Locati poll = queue.poll();
                System.out.println(poll.getMessage());
                for(Locati locati1 :poll.getNexts()){
                    queue.offer(locati1);
                }
            }
        }
//        if(locatis.size()==0){
//            return ;
//        }
//        for(code.Locati location:locatis){
//            System.out.println(location.getMessage());
//            disPlay(location.getNexts());
//        }
    }

    //处理message，返回一个locati对象，包含动作名称，其中输出动作包含！，输入动作包含？
    public static Locati handleMessOcSp(MessageOccurrenceSpecificationImpl messageOccurrenceSpecification){
        Locati locati =new Locati();
        String msgName="";
        msgName=messageOccurrenceSpecification.getMessage().getName();
        if(messageOccurrenceSpecification==messageOccurrenceSpecification.getMessage().getReceiveEvent()){
            msgName=msgName+"?";
        }
        if(messageOccurrenceSpecification==messageOccurrenceSpecification.getMessage().getSendEvent()){
            msgName=msgName+"!";
        }
        locati.setMessage(msgName);
        //System.out.println(msgName);
        return locati;
    }

    //处理opt组合片段，表示为locati对象，包含动作的名称以及进行该动作所需要的条件
    public static Locati handleCFopt(LifelineImpl lifeline, CombinedFragmentImpl cFopt){
        Locati locati =new Locati();
        String guard= cFopt.getOperands().get(0).getGuard().getSpecification().stringValue();
        //System.out.println(guard);
        String msgName="";
        for(InteractionFragment i:cFopt.getOperands().get(0).getFragments()){
            MessageOccurrenceSpecificationImpl MOs=(MessageOccurrenceSpecificationImpl)i;
            if(lifeline.getCoveredBys().contains(MOs)){
                msgName=MOs.getMessage().getName();
                if(MOs==MOs.getMessage().getReceiveEvent()){
                    msgName=msgName+"?";
                }
                if(MOs==MOs.getMessage().getSendEvent()){
                    msgName=msgName+"!";
                }
                locati.setMessage(msgName);
                //System.out.println(lifeline.getName()+"->"+msgName);
                locati.setGuard(guard);
            }
        }
        return locati;
    }

    //处理alt组合片段，表示为locati对象的一个列表，（分支），将Guard设置为c？（不懂）
    public static ArrayList<Locati> handleCFalt(Locati f, LifelineImpl lifeline, CombinedFragmentImpl cFalt){
        ArrayList<Locati> locatis =new ArrayList<>();
        for(InteractionOperand io:cFalt.getOperands()){
            //每个分支一个头节点一个指针
            ArrayList<Locati> head=new ArrayList<>();
            ArrayList<Locati> p=head;
            for(InteractionFragment i:io.getFragments()){
                if(i instanceof MessageOccurrenceSpecificationImpl){
                    MessageOccurrenceSpecificationImpl mos=(MessageOccurrenceSpecificationImpl) i;
                    if(lifeline.getCoveredBys().contains(mos)){
                        Locati locati =handleMessOcSp(mos);
                        locati.setGuard(mos.getEnclosingOperand().getGuard().getSpecification().stringValue());
                        p.add(locati);
                        p= locati.getNexts();
                        //p.add(f);
                    }
                }
                if(i instanceof CombinedFragmentImpl){
                    if("opt".equals(((CombinedFragmentImpl) i).getInteractionOperator().getName())){
                        Locati locati =handleCFopt(lifeline,(CombinedFragmentImpl) i);
                        p.add(locati);
                        p= locati.getNexts();
                        //p.add(f);
                    }
                    if("alt".equals(((CombinedFragmentImpl) i).getInteractionOperator().getName())){
                        Locati ff=new Locati();
                        ff.setGuard("c");
                        ArrayList<Locati> ls=handleCFalt(ff,lifeline,(CombinedFragmentImpl) i);
//                        if(((CombinedFragmentImpl) i)==io.getFragments().get(0)){
//                            for(code.Locati locati:ls){
//                                for(InteractionOperand io1:((CombinedFragmentImpl) i).getOperands()){
//                                    if(io1.)
//                                    locati.setGuard(io1.getGuard().getSpecification().stringValue());
//                                }
//
//                            }
//                        }
                        p.addAll(ls);
                        p=ff.getNexts();
                    }
                }
                //添加标志cFalt结束的节点
                if(io.getFragments().get(io.getFragments().size()-1)==i){
                    p.add(f);
                }
//                //添加guard值
//                for(code.Locati locati:head){
//                    locati.setGuard(io.getGuard().getSpecification().stringValue());
//                }
            }
            locatis.addAll(head);
        }
        return locatis;
    }

    //向文件中写入字符串
    public static void writeStrToFile(String str, String path) {
        try {
            //打开文件
            File file = new File(path);
            // 向文件写入对象写入信息
            FileWriter writer = new FileWriter(file,true);
            writer.write(str);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //将sysml顺序图转化为uppaal（需要改为转化为.si文件），
    public String SysML2UPPAAL(String str1, String str2) throws IOException {
        String path= str1;//相对路径
        TransformUtils.getSdModel(path);
        Document doc = new Document(new PrototypeDocument());
        for(InteractionImpl sd: TransformUtils.interactions){
            if (!sd.getName().equals(str2)){
                continue;
            }
            TransformUtils.getLifelineAndMessageByInteraction(sd);
            //System.out.println(code.TransformUtils.lifelines.size());
            //为每个interaction创建一个UPPAAL document
            //获取所有的消息名并添加为channel的全局声明
            String chan="chan ";
            for(MessageImpl message: TransformUtils.messages){
                chan = chan + message.getName() + ",";
            }
            chan=chan.substring(0,chan.length()-1);
            chan = chan+";";
            doc.setProperty("declaration",chan);

            //保存所有的模版对象
            ArrayList<Template> templates=new ArrayList<>();

            for(LifelineImpl lifeline: TransformUtils.lifelines){
                //添加TA 模版
                Template t = doc.createTemplate(); doc.insert(t, null);
                String module = sd.getName()+"_"+lifeline.getName();
                t.setProperty("name",module);
                templates.add(t);
                //System.out.println(code.TransformUtils.getAllCoveredByLifeline(sd,lifeline).size());

                //获取Location的内存图数据结构
                ArrayList<EObject> owned= TransformUtils.getAllCoveredByLifeline(sd,lifeline);
                //头节点
                ArrayList<Locati> head=new ArrayList<>();
                //指针p
                ArrayList<Locati> p=head;
                for(EObject e:owned){
                    if(e instanceof MessageOccurrenceSpecificationImpl){
                        Locati locati =handleMessOcSp((MessageOccurrenceSpecificationImpl) e);
                        p.add(locati);
                        p= locati.getNexts();
                    }
                    if(e instanceof CombinedFragmentImpl){
                        if("opt".equals(((CombinedFragmentImpl) e).getInteractionOperator().getName())){
                            Locati locati =handleCFopt(lifeline,((CombinedFragmentImpl) e));
                            if(null!= locati.getMessage()){
                                p.add(locati);
                                p= locati.getNexts();
                            }
                        }
                        if("alt".equals(((CombinedFragmentImpl) e).getInteractionOperator().getName())){
                            Locati f=new Locati();
                            f.setGuard("c");
                            ArrayList<Locati> locatis =handleCFalt(f,lifeline,((CombinedFragmentImpl) e));
                            p.addAll(locatis);
                            p=f.getNexts();
                        }
                    }
                }
                //处理locati的位置信息

                System.out.println("----"+lifeline.getName()+"----");
                //添加初始location
                Location init = t.createLocation();
                init.setProperty("init",true);
                t.insert(init, null);
                //获取所有的location，需要分层
                getAllLocation(head);
                //添加location 需要采用DAG布局算法
                HashMap<Locati,Location> map= TransformUtils.addLocation(t,locatis);
                for(Locati locati : locatis){
                    System.out.println(locati.getMessage() + ":"+locati.getGuard());
                }
                //添加边
                Location sec=map.get(locatis.get(0));
                Edge e = t.createEdge();
                t.insert(e, null);
                e.setSource(init);
                e.setTarget(sec);
                if(null!=locatis.get(0).getMessage()){
                    Property msg=e.setProperty("synchronisation",locatis.get(0).getMessage());
                    msg.setProperty("x",(sec.getX()- init.getX())/2);
                    msg.setProperty("y",(sec.getY()- init.getY())/2);
                    if(null!=locatis.get(0).getGuard()){
                        Property guard=e.setProperty("guard",locatis.get(0).getGuard());
                        guard.setProperty("x",(sec.getX()- init.getX())/2);
                        guard.setProperty("y",(sec.getY()- init.getY())/2-17);
                        //System.out.println(locatis.get(0).getGuard());
                    }
                }
                for(int i=0;i< locatis.size();i++){
                    for(int j=1;j<locatis.size();j++){
                        if(locatis.get(i).getNexts().contains(locatis.get(j))){
                            Location source=map.get(locatis.get(i));
                            Location target=map.get(locatis.get(j));
                            Edge edge = t.createEdge();
                            t.insert(edge, null);
                            edge.setSource(source);
                            edge.setTarget(target);
                            if(null!=locatis.get(j).getMessage()){
                                Property msg=edge.setProperty("synchronisation",locatis.get(j).getMessage());
                                msg.setProperty("x",(sec.getX()- init.getX())/2);
                                msg.setProperty("y",(sec.getY()- init.getY())/2);
                                if(null!=locatis.get(j).getGuard()){
                                    Property guard=edge.setProperty("guard",locatis.get(j).getGuard());
                                    guard.setProperty("x",(sec.getX()- init.getX())/2);
                                    guard.setProperty("y",(sec.getY()- init.getY())/2-17);
                                    //System.out.println(locatis.get(j).getGuard());
                                }
                            }
                        }
                    }
                }
                t.setProperty("declaration",sd.getName());
                //System.out.println("+++"+t.);
                System.out.println("-------------");
                break;
            }
            //添加模型声明
            doc.setProperty("system","system "+ TransformUtils.lifelines.get(0).getName()+";");
            //保存生成的xml文件
            doc.save("./examples/result.xml");
        }
        return "./examples/result.xml";
    }

    public static org.dom4j.Document getDocumentByPath(String path) throws DocumentException, IOException {
        SAXReader saxReader = new SAXReader();
        File file = new File(path);
        FileInputStream intput = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(intput));
        String tempString;//定义一个字符串，每一次读出该行字符串内容
        List<String> list = new ArrayList<>();//定义一个list字符串集合用来储存每一行的字符串信息
        //删除dtd声明
        while ((tempString = reader.readLine()) != null) {
            if (!"<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd'>".equals(tempString)){
                list.add(tempString);
            }
        }

        FileWriter fd = new FileWriter(file, false);//append传入false表示写入内容时将会覆盖文件中之前存在的内容
        fd.write("");//执行删除操作，写入空内容覆盖之前的内容
        fd.close();
        FileWriter fw = new FileWriter(file, true);
        for (String s : list){
            fw.write(s);
            fw.write(System.getProperty("line.separator"));
            //System.out.println(s);
        }
        fw.close();
        org.dom4j.Document document = saxReader.read(file);
        return document;
    }

    public static org.dom4j.Element getTemplate(org.dom4j.Document doc){
        org.dom4j.Element template = null;
        template = doc.getRootElement().element("template");
        return template;
    }

    public static ArrayList<org.dom4j.Element> getAllLocationsInTemplate(org.dom4j.Element template){
        ArrayList<org.dom4j.Element> ret = new ArrayList<>();
        Iterator locations = template.elementIterator("location");
        while(locations.hasNext()){
            org.dom4j.Element location = (org.dom4j.Element) locations.next();
            ret.add(location);
        }
        return ret;
    }

    public static ArrayList<org.dom4j.Element> getAllTransitionInTemplate(org.dom4j.Element template){
        ArrayList<org.dom4j.Element> ret = new ArrayList<>();
        Iterator transitions = template.elementIterator("transition");
        while(transitions.hasNext()){
            org.dom4j.Element transition = (org.dom4j.Element) transitions.next();
            ret.add(transition);
        }
        return ret;
    }

    public static void Transform(String pathname, String interactioname) throws IOException, DocumentException {
        Transform transform=new Transform();
        String uppaal = transform.SysML2UPPAAL(pathname, interactioname);
        org.dom4j.Document document = getDocumentByPath(uppaal);
        org.dom4j.Element template = getTemplate(document);
        ArrayList<org.dom4j.Element> locations = getAllLocationsInTemplate(template);
        Integer count = 0;
        //添加name
        for(org.dom4j.Element location : locations){
            org.dom4j.Element name = DocumentHelper.createElement("name");
            name.setText("s"+(count++));
            if(!location.elements().isEmpty()){
                location.elements().add(0,name);
            }else{
                location.elements().add(name);
            }
        }
        //添加不变量
        ArrayList<org.dom4j.Element> transitions = getAllTransitionInTemplate(template);
        //System.out.println(transitions.size());
        for(org.dom4j.Element location : locations){
            String sourceid = location.attributeValue("id");
            //System.out.println(sourceid);
            for(org.dom4j.Element transition : transitions){
                if(sourceid.equals(transition.element("source").attributeValue("ref"))){
                    List<org.dom4j.Element> labels = transition.elements("label");
                     if(!labels.isEmpty()){
                        for(org.dom4j.Element label : labels){
                            if(label.attributeValue("kind").equals("guard")){
                                org.dom4j.Element newlabel = DocumentHelper.createElement("label");
                                newlabel.addAttribute("kind","invariant");
                                newlabel.setText(label.getText());
                                //System.out.println(label.getText());
                                location.elements().add(newlabel);
                            }
                        }
                    }
                }
            }
        }

        for(org.dom4j.Element location : locations){
            if(location.elements("label").size()>1){
                Integer max = 0;
                for(org.dom4j.Element label : location.elements("label")){
                    if(label.getText().contains("c<=")){
                        if(max <= Integer.valueOf(label.getText().substring(3,label.getText().length()))){
                            max = Integer.valueOf(label.getText().substring(3,label.getText().length()));
                        }
                    }
                }
                for(org.dom4j.Element label : location.elements("label")){
                    location.remove(label);
                }
                org.dom4j.Element newlabel = DocumentHelper.createElement("label");
                newlabel.addAttribute("kind","invariant");
                newlabel.setText("c<="+max);
                location.elements().add(newlabel);
            }
        }

        //写文件
        OutputFormat format = new OutputFormat();
        format.setIndentSize(2);
        format.setNewlines(true);
        format.setTrimText(true);
        format.setPadText(true);
        format.setEncoding("utf-8");
        FileOutputStream file =  new FileOutputStream(uppaal);
        XMLWriter writer = new XMLWriter(file, format);
        writer.write(document);
        writer.close();

        //生成.si文件
        String declaration = null;
        String name = null;

        declaration = template.element("declaration").getText();
        name = template.element("name").getText();

        String si = "./examples/"+declaration+".si";
        File file1 = new File(si);
        FileWriter fd = new FileWriter(file1, false);//append传入false表示写入内容时将会覆盖文件中之前存在的内容
        fd.write("");//执行删除操作，写入空内容覆盖之前的内容
        fd.close();
        FileWriter fw = new FileWriter(file1, true);

        //如果args[2]为1，则代表为接口自动机结构，则向.si文档中写入“module-iots ”+name+":"，否则为接口安全结构，写入”module “+name+":";然后换行；
        /*if(args[2].equals("1")){
            fw.write("module-iots "+name+":");
        }else{
            fw.write("module "+name+":");
        }*/
        fw.write("module "+name+":");

        fw.write(System.getProperty("line.separator"));
        fw.close();

        //获取init，向.si文档中写入“initial : ”+init，然后换行；
        String init = template.element("init").attributeValue("ref");
        for(org.dom4j.Element location : locations){
            if(location.attributeValue("id").equals(init)){
                FileWriter fw1 = new FileWriter(file1, true);
                fw1.write("initial : "+location.element("name").getText());
                fw1.write(System.getProperty("line.separator"));
                fw1.close();
            }
        }

        for(org.dom4j.Element transition : transitions){
            List<org.dom4j.Element> labels = transition.elements("label");
            if(!labels.isEmpty()){
                for(org.dom4j.Element label : labels){
                    if(label.attributeValue("kind").equals("synchronisation")){
                        String labeltext = label.getText();
                        if(labeltext.endsWith("?")){
                            FileWriter fw1 = new FileWriter(file1, true);
                            fw1.write("input "+labeltext.substring(0,labeltext.length()-1)+" : ");
                            fw1.close();
                        }else{
                            FileWriter fw1 = new FileWriter(file1, true);
                            fw1.write("output "+labeltext.substring(0,labeltext.length()-1)+" : ");
                            fw1.close();
                        }
                    }
                }
                String source = transition.element("source").attributeValue("ref");
                String target = transition.element("target").attributeValue("ref");
                for(org.dom4j.Element location : locations){
                    if(source.equals(location.attributeValue("id"))){
                        source = location.element("name").getText();
                    }
                    if(target.equals(location.attributeValue("id"))){
                        target = location.element("name").getText();
                    }
                }
                FileWriter fw1 = new FileWriter(file1, true);
                fw1.write("{"+source+" -> "+target+"}");
                fw1.write(System.getProperty("line.separator"));
                fw1.close();
            }
        }
        FileWriter fw1 = new FileWriter(file1, true);
        fw1.write("endmodule");
        fw1.write(System.getProperty("line.separator"));
        fw1.close();

    }
    /*public static void main(String args[]) throws Exception{
        Transform Trans = new Transform();
        Trans.Transform("./modeling_workspace/TestNewsysmlia/mode_switch_sec.uml", "stabilize_mode_switch_sec");
    }*/
}
