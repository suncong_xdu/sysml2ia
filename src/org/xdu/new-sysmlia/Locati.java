package core;

import java.util.ArrayList;

public class Locati {

    private String message;
    private String guard;
    private ArrayList<Locati> nexts=new ArrayList<>();

    public Locati() {
    }

    public Locati(String message, String guard) {
        this.message = message;
        this.guard = guard;
    }

    public void setNexts(ArrayList<Locati> nexts) {
        this.nexts = nexts;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGuard() {
        return guard;
    }

    public void setGuard(String guard) {
        this.guard =guard;
    }

    public ArrayList<Locati> getNexts() {
        return nexts;
    }

    public void addNexts(Locati next) {
        this.nexts.add(next) ;
    }
}
