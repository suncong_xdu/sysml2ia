package core;

import com.uppaal.model.core2.Location;
import com.uppaal.model.core2.Template;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.internal.impl.*;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class TransformUtils {

    public static ArrayList<InteractionImpl> interactions=new ArrayList<>();
    public static ArrayList<LifelineImpl> lifelines;
    public static ArrayList<MessageImpl> messages;

    /**
     * 保存uml文件下的所有interaction
     * @param path
     * @return
     */
    public static void getSdModel(String path){
        //创建URI
        URI uri = URI.createURI(path);
        ResourceSet set = new ResourceSetImpl();
        /**
         * UMLPackage.eNS_URI：包命名空间的URI
         *UMLPackage.eINSTANCE：包的单例实例
         * set.getPackageRegistry()：返回用于查找基于包的命名空间的注册器
         * set.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE)：
         * 将包命名空间的URI与包的单例实例作为键值对添加到注册器；
         */
        set.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
        /**
         * UMLResource：关联到uml包的资源
         * UMLResource.FILE_EXTENSION：文件扩展名"uml"
         * UMLResource.Factory：uml资源工厂类
         * UMLResource.Factory.INSTANCE：uml资源工厂类实例
         *set.getResourceFactoryRegistry()：返回用于创建适当类型资源的注册器
         *getExtensionToFactoryMap()：返回一个map用于保存文件扩展和资源工厂的实例的映射
         * 注册资源工厂
         */
        set.getResourceFactoryRegistry().getExtensionToFactoryMap()
                .put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);

        UMLResourcesUtil.init(set);
        /**
         *Resource.Factory.Registry.INSTANCE：全局静态资源工厂注册器实例
         *注册资源工厂到全局静态资源工厂注册器实例
         */
//        UMLResource.Factory.Registry.INSTANCE.getExtensionToFactoryMap()
//                .put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap()
                .put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);

        //返回由 URI 解析的资源
        Resource res = set.getResource(uri, true);
        EObject root=res.getContents().get(0);
        //EObject sd=null;
        for(EObject e:root.eContents()){
            //System.out.println(e);
            if("Interaction".equals(e.eClass().getName())){
                interactions.add((InteractionImpl) e);
            }
        }
    }

    public static void getLifelineAndMessageByInteraction(InteractionImpl sd){
        lifelines=new ArrayList<>();
        messages=new ArrayList<>();
        for (EObject e: sd.eContents()) {
            if(e instanceof LifelineImpl){
                lifelines.add((LifelineImpl) e);
            }
            if(e instanceof MessageImpl){
                messages.add((MessageImpl) e);
            }
        }
    }

    public static ArrayList<EObject> getAllCoveredByLifeline(InteractionImpl sd,LifelineImpl lifeline){
        ArrayList<EObject> ret=new ArrayList<>();
        for (EObject e: sd.eContents()) {
            if(e instanceof  MessageOccurrenceSpecificationImpl){
                if(lifeline.getCoveredBys().contains((MessageOccurrenceSpecificationImpl) e)){
                    ret.add((MessageOccurrenceSpecificationImpl) e);
                }
            }
//            if(e instanceof ExecutionOccurrenceSpecificationImpl){
//                if(lifeline.getCoveredBys().contains((ExecutionOccurrenceSpecificationImpl) e)){
//                    ret.add((ExecutionOccurrenceSpecificationImpl) e);
//                }
//            }
//            if(e instanceof ActionExecutionSpecificationImpl){
//                if(lifeline.getCoveredBys().contains((ActionExecutionSpecificationImpl) e)){
//                    ret.add((ActionExecutionSpecificationImpl) e);
//                }
//            }
            if(e instanceof CombinedFragmentImpl){
                if(lifeline.getCoveredBys().contains((CombinedFragmentImpl) e)){
                    ret.add((CombinedFragmentImpl) e);
                }
            }
        }
        return ret;
    }

    public static HashMap<Locati,Location> addLocation(Template t, ArrayList<Locati> locatis){
        HashMap<Locati,Location> ret=new HashMap<>();
        for(Locati locati:locatis){
            Location l = t.createLocation();
            if("c".equals(locati.getGuard())){
                //System.out.println(locati.getGuard());
                //l.setProperty("committed",true);
            }
            t.insert(l, null);
            ret.put(locati,l);
        }
        return ret;
    }


}
