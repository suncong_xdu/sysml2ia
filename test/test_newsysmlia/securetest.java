package test.test_newsysmlia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;
import org.xdu.nni.StrictInputRefine;

class ParseIndex {
    private String pathName;

    public ParseIndex() {
    }

    public ParseIndex(String pathName) {
        this.pathName = pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getPathName() {
        return this.pathName;
    }

    public int getIndex(String key) {
        int index = -1;
        boolean matched = false;

        try {
            BufferedReader file = new BufferedReader(new FileReader(this.pathName));

            String str;
            while((str = file.readLine()) != null) {
                if (str.startsWith("module")) {
                    if (str.endsWith(key + ":")) {
                        ++index;
                        matched = true;
                        break;
                    }

                    matched = false;
                    ++index;
                }
            }

            file.close();
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        return index >= 0 && matched ? index : -1;
    }
}

class ValidCheck {
    private String fileName;
    private String interfaceName;

    public ValidCheck() {
    }

    public ValidCheck(String fileName, String interfaceName) {
        this.fileName = fileName;
        this.interfaceName = interfaceName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getInterfaceName() {
        return this.interfaceName;
    }

    public boolean verif() {
        ParseIndex pi = new ParseIndex(this.fileName);
        int indexOfInterface = pi.getIndex(this.interfaceName);
        IAParser parser = new IAParser();
        parser.ConstructIA("./examples/sec_temp.si");
        InterfaceAutomaton ia = (InterfaceAutomaton)parser.getInterfaceAutomatons().get(indexOfInterface);
        String[] lattice = new String[]{"L", "H"};
        SecIA sec_ia = new SecIA(ia, lattice);

        for(int i = lattice.length - 2; i >= 0; --i) {
            sec_ia.GenerateBothSidesAtLevel(lattice[i]);
        }

        Iterator var8 = sec_ia.getIAPairList().iterator();

        while(var8.hasNext()) {
            IAPair iap = (IAPair)var8.next();
            iap.setDecision((new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()));

            try {
                PrintStream out = System.out;
                PrintStream ps = new PrintStream("./examples/temp");
                System.setOut(ps);
                iap.Print();
                System.setOut(out);
            } catch (FileNotFoundException var11) {
                var11.printStackTrace();
            }
        }

        String resultLine = "";

        try {
            BufferedReader file = new BufferedReader(new FileReader("./examples/temp"));

            String line;
            while((line = file.readLine()) != null) {
                if (line.startsWith("Decision")) {
                    resultLine = resultLine + line;
                }
            }

            file.close();
        } catch (IOException var12) {
            var12.printStackTrace();
        }

        return resultLine.endsWith("true");
    }

    public void secConvert() {
        String resultLine = "";

        try {
            BufferedReader file = new BufferedReader(new FileReader(this.fileName));

            while(true) {
                String line;
                while((line = file.readLine()) != null) {
                    if (!line.startsWith("input") && !line.startsWith("output")) {
                        resultLine = resultLine + line + "\n";
                    } else {
                        int index;
                        if (line.contains("_HI_")) {
                            index = line.indexOf("_HI_");
                            resultLine = resultLine + line.substring(0, index);
                            resultLine = resultLine + "_L_";
                            resultLine = resultLine + line.substring(index + 4, line.length());
                            resultLine = resultLine + "\n";
                        } else if (line.contains("_LI_")) {
                            index = line.indexOf("_LI_");
                            resultLine = resultLine + line.substring(0, index);
                            resultLine = resultLine + "_H_";
                            resultLine = resultLine + line.substring(index + 4, line.length());
                            resultLine = resultLine + "\n";
                        } else if (line.contains("_LC_")) {
                            index = line.indexOf("_LC_");
                            resultLine = resultLine + line.substring(0, index);
                            resultLine = resultLine + "_L_";
                            resultLine = resultLine + line.substring(index + 4, line.length());
                            resultLine = resultLine + "\n";
                        } else if (line.contains("_HC_")) {
                            index = line.indexOf("_HC_");
                            resultLine = resultLine + line.substring(0, index);
                            resultLine = resultLine + "_H_";
                            resultLine = resultLine + line.substring(index + 4, line.length());
                            resultLine = resultLine + "\n";
                        } else {
                            System.out.println(this.fileName);
                            System.out.println("Error!");
                        }
                    }
                }

                file.close();
                BufferedWriter filewrite = new BufferedWriter(new FileWriter("./examples/sec_temp.si"));
                filewrite.write(resultLine);
                filewrite.close();
                break;
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }
}


public class securetest {
    public static void secure(String pathname, String interactionname) {
        ValidCheck vc = new ValidCheck(pathname, interactionname);
        vc.secConvert();
        System.out.println("Verifing: " + interactionname);
        System.out.print("Verif result: ");
        System.out.println(vc.verif());
    }

    public static void main(String[] args) {
        //不满足数据流安全验证
        //secure("./examples/stabilize_mode_switch_sec.si", "stabilize_mode_switch_sec_Copter");
        //满足数据流安全验证
        secure("./examples/stabilize_mode_switch_sec2.si", "stabilize_mode_switch_sec2_Copter");
    }
}
