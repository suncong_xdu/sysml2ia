package test.test_newsysmlia;

import java.io.*;
import java.util.*;
import core.*;
import org.dom4j.DocumentException;


public class newsysmlia {
    //type为要转换的类型（接口自动机iots，接口安全结构sts），pathname为要转换的文件路径，interactionname为要转换的interaction名称
    public static void test( String pathname, String interactionname) throws IOException, DocumentException {
        Transform Trans = new Transform();
        Trans.Transform(pathname, interactionname);
    }
    public static void main(String[] args) throws IOException, DocumentException {
        test("./modeling_workspace/mode_switch_sec.uml", "stabilize_mode_switch_sec");
        test("./modeling_workspace/mode_switch_sec2.uml", "stabilize_mode_switch_sec2");
        test("./modeling_workspace/mode_switch_spec.uml", "stabilize_mode_switch_spec");
        test("./modeling_workspace/mode_switch_impl.uml", "stabilize_mode_switch_impl");
        test("./modeling_workspace/mode_switch_impl_update.uml", "stabilize_mode_switch_impl_update");
    }
}
