
package cycab;

//import org.xdu.jia.IAParser;

import org.xdu.jia.*;
import org.xdu.nni.*;
import org.xdu.sysmlia.*;

public class TestCycab{

	public static void Test1(){
		IAGenerator gen=new IAGenerator();
		gen.GenerateIAs("../modeling_workspace/CyCab/model.uml");

		IAParser parser=new IAParser();
		parser.ConstructIA("../modeling_workspace/CyCab/model.si");
		/*for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			ia.Print();
		}
		System.out.println("********************************************");*/
		
		String[] trilattice=new String[]{"L","M","H"};
		SecIA sec_ia;
		
		InterfaceAutomaton Starter=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton EmergHalt=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton VC1=parser.getInterfaceAutomatons().get(2);
		InterfaceAutomaton VC2=parser.getInterfaceAutomatons().get(3);
		InterfaceAutomaton VC3=parser.getInterfaceAutomatons().get(4);
		InterfaceAutomaton VC4=parser.getInterfaceAutomatons().get(5);
		InterfaceAutomaton Sensor = parser.getInterfaceAutomatons().get(6);
		InterfaceAutomaton ComputingUnit= parser.getInterfaceAutomatons().get(7);
		
		System.out.println("Starter: secure");
		sec_ia=new SecIA(Starter, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		System.out.println("EmergHalt: secure");
		sec_ia=new SecIA(EmergHalt, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		System.out.println("Computing Unit: insecure");
		sec_ia=new SecIA(ComputingUnit, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		System.out.println("Sensor: secure");
		sec_ia=new SecIA(Sensor, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}

		System.out.println("Vehicle Core: ");
		System.out.println("Vehicle Core Interface with Station 1: secure");
		sec_ia=new SecIA(VC1, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		System.out.println("Vehicle Core Interface with Station 2: secure");
		sec_ia=new SecIA(VC2, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		System.out.println("Vehicle Core Interface with Starter: secure");
		sec_ia=new SecIA(VC3, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		System.out.println("Vehicle Core Interface with EmergHalt: secure");
		sec_ia=new SecIA(VC4, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}

	}
	
	// This test case treats the vehicle core as an individual interface
	private static void Test2(){
		IAParser parser=new IAParser();
		parser.ConstructIA("../examples/Vehicle.si");
		
		String[] trilattice=new String[]{"L","M","H"};
		SecIA sec_ia;
		
		InterfaceAutomaton VC=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton CU=parser.getInterfaceAutomatons().get(1);
		
		sec_ia=new SecIA(VC, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		sec_ia=new SecIA(CU, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		InterfaceAutomaton VC_CU=VC.Composition(CU);
		sec_ia=new SecIA(VC_CU, trilattice);
		for(int i=trilattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(trilattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
	}
	
	public static void main(String[] args){
		Test1();
		//Test2();
	}
}