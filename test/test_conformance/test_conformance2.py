import subprocess

def run_command(command):
    try:
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, check=True)
        print("命令执行成功，标准输出如下：")
        print(result.stdout)
    except subprocess.CalledProcessError as e:
        print("命令执行失败，错误消息如下：")
        print(e.stderr)
    except Exception as e:
        print("发生未知错误:", str(e))

#-test参数用于测试程序，指定输出的测试用例集文件名称,默认为testcases.txt
def main():
    #测试用例2，测试stabilize_mode_switch_impl.si和stabilize_mode_switch_impl2_update.si的一致性,结果应为True
    command2 = "python ../../src/org/xdu/conformance_verify/main.py --conformance -s ../../examples/stabilize_mode_switch_spec.si -i ../../examples/stabilize_mode_switch_impl_update.si -test ./testcases2.txt"

    run_command(command2)

if __name__ == "__main__":
    main()