package bisim;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.Bisim;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;

import java.io.*;
import java.util.*;

public class BisimCases{
	public static void BisimCheck(String pathname, String[] binlattice) {
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		
		List<InterfaceAutomaton> automata=parser.getInterfaceAutomatons();
		
		for(InterfaceAutomaton automaton: automata) {
			SecIA sec_ia=new SecIA(automaton, binlattice);
			//binlattice[binlattice.length-1] is the highest level, needn't to apply the decision procedure on this level.
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new Bisim()).decide(iap.getLeft(), iap.getRight()) );
				iap.Print();
			}
		}
	}
	
	public static void main(String[] args){
		String[] binlattice=new String[]{"L","H"};
		
		//BisimCheck("../examples/Lee_SCCC_Fig3.si", binlattice);
		//BisimCheck("../examples/Lee_SCCC_Fig1.si", binlattice);
		BisimCheck("../examples/Lee_SCCC_Fig2.si", binlattice);
	}
}