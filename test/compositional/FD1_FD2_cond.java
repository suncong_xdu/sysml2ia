package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class FD1_FD2_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/FireDetector.si");
		
		InterfaceAutomaton FireD1=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton FireD2=parser.getInterfaceAutomatons().get(1);
		
		long startTime;
		long endTime;
		
		//FireD1*FireD2 sec cond:
		//System.out.println((new CompositionalCheck()).HasReachableErrorStates(FireD1, FireD2));
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(FireD1, FireD2);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("FireD1*FireD2 sec cond:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}