package compositional.sir;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;
import org.xdu.nni.StrictInputRefine;

public class FD2_SIR{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/FireDetector.si");
		String[] binlattice=new String[]{"L","H"};

		InterfaceAutomaton FireD2=parser.getInterfaceAutomatons().get(1);
		
		long startTime;
		long endTime;
		SecIA sec_ia;
		
		//FireD2:
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			sec_ia=new SecIA(FireD2, binlattice);
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
				//iap.Print();
			}
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("FireD2:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}