package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class SV_TPU_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/Lee_ENTCS_Fig1.si");
		
		InterfaceAutomaton SV=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton TPU=parser.getInterfaceAutomatons().get(2);
		
		long startTime;
		long endTime;
		
		//SV*TPU sec cond:
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(SV, TPU);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("SV*TPU sec condition:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}