package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class CtrlU_FD1_FD2_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/FireDetector.si");
		
		InterfaceAutomaton FireD1=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton FireD2=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton CtrlU=parser.getInterfaceAutomatons().get(2);
		
		long startTime;
		long endTime;
		
		InterfaceAutomaton FireD1_FireD2=FireD1.Composition(FireD2);
		//(FireD1*FireD2)*CtrlU sec cond:
		//System.out.println((new CompositionalCheck()).HasReachableErrorStates(FireD1_FireD2, CtrlU));
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(FireD1_FireD2, CtrlU);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("CtrlU*(FireD1*FireD2) sec cond:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}