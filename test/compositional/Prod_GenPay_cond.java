package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class Prod_GenPay_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/ProdPay.si");
		
		InterfaceAutomaton Prod=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton GenPay=parser.getInterfaceAutomatons().get(2);
		
		long startTime;
		long endTime;
		
		//Prod*GenPay sec cond
		//System.out.println((new CompositionalCheck()).HasReachableErrorStates(Prod, GenPay));
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(Prod, GenPay);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("Prod*GenPay sec cond:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}