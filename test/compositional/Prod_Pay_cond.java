package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class Prod_Pay_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/ProdPay.si");
		
		InterfaceAutomaton Prod=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton Pay=parser.getInterfaceAutomatons().get(1);
		
		long startTime;
		long endTime;
		
		//Prod*Pay sec cond
		//System.out.println((new CompositionalCheck()).HasReachableErrorStates(Prod, Pay));
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(Prod, Pay);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("Prod*Pay sec cond:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}