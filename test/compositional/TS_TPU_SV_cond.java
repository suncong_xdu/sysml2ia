package compositional;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;

public class TS_TPU_SV_cond{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/Lee_ENTCS_Fig1.si");
		
		InterfaceAutomaton TS=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton SV=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton TPU=parser.getInterfaceAutomatons().get(2);
		
		long startTime;
		long endTime;
		
		//TS*TPU*SV sec cond
		InterfaceAutomaton TS_TPU=TS.Composition(TPU);
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			(new CompositionalCheck()).HasReachableErrorStates(TS_TPU, SV);
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("TS*TPU*SV sec cond:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}