package compositional.bisim;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.Bisim;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;

public class Prod_Pay_Bisim{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/ProdPay.si");
		String[] binlattice=new String[]{"L","H"};
		
		InterfaceAutomaton Prod=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton Pay=parser.getInterfaceAutomatons().get(1);
		
		long startTime;
		long endTime;
		SecIA sec_ia;
		
		//Prod*Pay whole
		InterfaceAutomaton ProdPay=Prod.Composition(Pay);
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			sec_ia=new SecIA(ProdPay, binlattice);
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new Bisim()).decide(iap.getLeft(), iap.getRight()) );
				//iap.Print();
			}
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("Prod*Pay whole:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}