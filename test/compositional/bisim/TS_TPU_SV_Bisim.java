package compositional.bisim;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.Bisim;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;

public class TS_TPU_SV_Bisim{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/Lee_ENTCS_Fig1.si");
		String[] binlattice=new String[]{"L","H"};
		
		InterfaceAutomaton TS=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton SV=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton TPU=parser.getInterfaceAutomatons().get(2);
		
		long startTime;
		long endTime;
		SecIA sec_ia;
		
		//TS*TPU*SV whole
		InterfaceAutomaton TS_TPU_SV=(TS.Composition(TPU)).Composition(SV);
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			sec_ia=new SecIA(TS_TPU_SV, binlattice);
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new Bisim()).decide(iap.getLeft(), iap.getRight()) );
				//iap.Print();
			}
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("TS*TPU*SV whole:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}