package compositional.bisim;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.Bisim;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;

public class CtrlU_FD1_FD2_Bisim{
	private static final int TIMES=1000;
	
	private static Runtime rt = Runtime.getRuntime();
	private static long maxMemoryCost=0;
	private static void peekMemoryCost(){
		long currentMemoryCost = rt.totalMemory()-rt.freeMemory();
		if( currentMemoryCost > maxMemoryCost)
			maxMemoryCost=currentMemoryCost;
	}
	private static void printMemoryCost(){
		System.out.println("Memory cost: "+Math.round((double)(maxMemoryCost)/1024) + " kB");
	}
	
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("examples/FireDetector.si");
		String[] binlattice=new String[]{"L","H"};
		
		InterfaceAutomaton FireD1=parser.getInterfaceAutomatons().get(0);
		InterfaceAutomaton FireD2=parser.getInterfaceAutomatons().get(1);
		InterfaceAutomaton CtrlU=parser.getInterfaceAutomatons().get(2);
				
		long startTime;
		long endTime;
		SecIA sec_ia;
		
		//CtrlU*(FireD1*FireD2) whole:
		InterfaceAutomaton FireD1_FireD2=FireD1.Composition(FireD2);
		InterfaceAutomaton FireD1_FireD2_CtrlU=CtrlU.Composition(FireD1_FireD2);
		startTime = System.currentTimeMillis();
		for(int j=0;j<TIMES;j++){
			sec_ia=new SecIA(FireD1_FireD2_CtrlU, binlattice);
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new Bisim()).decide(iap.getLeft(), iap.getRight()) );
				//iap.Print();
			}
			peekMemoryCost();
		}
		endTime=System.currentTimeMillis();
		System.out.println("CtrlU*(FireD1*FireD2) whole:"+(endTime-startTime)+" ms");
		
		printMemoryCost();
	}
}