package sir;

import java.io.File;
import java.util.List;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;
import org.xdu.nni.StrictInputRefine;

public class StrictInputRefineCase1{
	
	public static void SIRCheck(String pathname, String[] binlattice) {
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		
		List<InterfaceAutomaton> automata=parser.getInterfaceAutomatons();
		
		for(InterfaceAutomaton automaton: automata) {
			SecIA sec_ia=new SecIA(automaton, binlattice);
			
			//binlattice[binlattice.length-1] is the highest level, needn't to apply the decision procedure on this level.
			for(int i=binlattice.length-2;i>=0;i--){
				sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
			}
			for(IAPair iap: sec_ia.getIAPairList() ){
				iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
				iap.Print();
			}

		}
	}

	public static void main(String[] args){
		String[] binlattice=new String[]{"L","H"};
		
		SIRCheck("../examples/Lee_SCCC_Fig3.si", binlattice);
		//SIRCheck("../examples/Lee_SCCC_Fig1.si", binlattice);
		//SIRCheck("../examples/Lee_SCCC_Fig2.si", binlattice);
		//SIRCheck("../examples/Lee_SCCC_Fig4.si", binlattice);
	}
}