package sir;

import org.xdu.jia.IAParser;
import org.xdu.jia.InterfaceAutomaton;
import org.xdu.nni.CompositionalCheck;
import org.xdu.nni.IAPair;
import org.xdu.nni.SecIA;
import org.xdu.nni.StrictInputRefine;

public class StrictInputRefineCase5{
	public static void main(String[] args){
		IAParser parser=new IAParser();
		parser.ConstructIA("../examples/Lee_SCCC_Fig5.si");
		String[] binlattice=new String[]{"L","H"};
		InterfaceAutomaton S= (parser.getInterfaceAutomatons()).get(0);
		InterfaceAutomaton T= (parser.getInterfaceAutomatons()).get(1);
		//S.Print();
		//T.Print();
		SecIA sec_ia=new SecIA(S,binlattice);
		for(int i=binlattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		sec_ia=new SecIA(T,binlattice);
		for(int i=binlattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		sec_ia=new SecIA(S.Composition(T), binlattice);
		for(int i=binlattice.length-2;i>=0;i--){
			sec_ia.GenerateBothSidesAtLevel(binlattice[i]);
		}
		for(IAPair iap: sec_ia.getIAPairList() ){
			iap.setDecision( (new StrictInputRefine()).decide(iap.getLeft(), iap.getRight()) );
			iap.Print();
		}
		
		System.out.print("Has reachable error state: ");
		System.out.println( (new CompositionalCheck()).HasReachableErrorStates(S, T) );
	}
}